-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: megahor
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `course_plan_information`
--

DROP TABLE IF EXISTS `course_plan_information`;
/*!50001 DROP VIEW IF EXISTS `course_plan_information`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_plan_information` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_information`
--

DROP TABLE IF EXISTS `course_information`;
/*!50001 DROP VIEW IF EXISTS `course_information`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_information` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `email_teach`,
 1 AS `id_unit`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_information_names`
--

DROP TABLE IF EXISTS `course_information_names`;
/*!50001 DROP VIEW IF EXISTS `course_information_names`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_information_names` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `email_teach`,
 1 AS `id_unit`,
 1 AS `name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_plan_teachers`
--

DROP TABLE IF EXISTS `course_plan_teachers`;
/*!50001 DROP VIEW IF EXISTS `course_plan_teachers`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_plan_teachers` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `email_teacher`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `present_unitname`
--

DROP TABLE IF EXISTS `present_unitname`;
/*!50001 DROP VIEW IF EXISTS `present_unitname`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `present_unitname` AS SELECT 
 1 AS `name`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `id_course`,
 1 AS `id_unit`,
 1 AS `unit_name`,
 1 AS `color`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_plan_classes`
--

DROP TABLE IF EXISTS `course_plan_classes`;
/*!50001 DROP VIEW IF EXISTS `course_plan_classes`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_plan_classes` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `class_name`,
 1 AS `class_type`,
 1 AS `schedule`,
 1 AS `schedule_url`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_plan_teachersnames`
--

DROP TABLE IF EXISTS `course_plan_teachersnames`;
/*!50001 DROP VIEW IF EXISTS `course_plan_teachersnames`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_plan_teachersnames` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `email_teacher`,
 1 AS `name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_plan_units`
--

DROP TABLE IF EXISTS `course_plan_units`;
/*!50001 DROP VIEW IF EXISTS `course_plan_units`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_plan_units` AS SELECT 
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `id_course`,
 1 AS `id`,
 1 AS `unit_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `unitsnamestoclasses`
--

DROP TABLE IF EXISTS `unitsnamestoclasses`;
/*!50001 DROP VIEW IF EXISTS `unitsnamestoclasses`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `unitsnamestoclasses` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `class_name`,
 1 AS `id_unit`,
 1 AS `unit_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_information_units`
--

DROP TABLE IF EXISTS `course_information_units`;
/*!50001 DROP VIEW IF EXISTS `course_information_units`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_information_units` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `email_teach`,
 1 AS `id_unit`,
 1 AS `unit_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!50001 DROP VIEW IF EXISTS `teachers`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `teachers` AS SELECT 
 1 AS `email`,
 1 AS `password`,
 1 AS `name`,
 1 AS `number`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `unitstoclasses`
--

DROP TABLE IF EXISTS `unitstoclasses`;
/*!50001 DROP VIEW IF EXISTS `unitstoclasses`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `unitstoclasses` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `class_name`,
 1 AS `id_unit`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `course_information_classes`
--

DROP TABLE IF EXISTS `course_information_classes`;
/*!50001 DROP VIEW IF EXISTS `course_information_classes`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `course_information_classes` AS SELECT 
 1 AS `id`,
 1 AS `course_name`,
 1 AS `email`,
 1 AS `school_year`,
 1 AS `semester`,
 1 AS `unaval_stain_interval`,
 1 AS `id_course`,
 1 AS `initial_hour`,
 1 AS `final_hour`,
 1 AS `block_duration`,
 1 AS `block_for_unit`,
 1 AS `class_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `course_plan_information`
--

/*!50001 DROP VIEW IF EXISTS `course_plan_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_plan_information` AS select `course`.`id` AS `id`,`course`.`course_name` AS `course_name`,`course`.`email` AS `email`,`course_plan`.`school_year` AS `school_year`,`course_plan`.`semester` AS `semester`,`course_plan`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_plan`.`id_course` AS `id_course`,`course_plan`.`initial_hour` AS `initial_hour`,`course_plan`.`final_hour` AS `final_hour`,`course_plan`.`block_duration` AS `block_duration`,`course_plan`.`block_for_unit` AS `block_for_unit` from (`course` join `course_plan` on((`course`.`id` = `course_plan`.`id_course`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_information`
--

/*!50001 DROP VIEW IF EXISTS `course_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_information` AS select `course_plan_information`.`id` AS `id`,`course_plan_information`.`course_name` AS `course_name`,`course_plan_information`.`email` AS `email`,`course_plan_information`.`school_year` AS `school_year`,`course_plan_information`.`semester` AS `semester`,`course_plan_information`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_plan_information`.`id_course` AS `id_course`,`course_plan_information`.`initial_hour` AS `initial_hour`,`course_plan_information`.`final_hour` AS `final_hour`,`course_plan_information`.`block_duration` AS `block_duration`,`course_plan_information`.`block_for_unit` AS `block_for_unit`,`teaches`.`email_teach` AS `email_teach`,`teaches`.`id_unit` AS `id_unit` from (`course_plan_information` join `teaches` on(((`course_plan_information`.`id_course` = `teaches`.`id_course`) and (`course_plan_information`.`school_year` = `teaches`.`school_year`) and (`course_plan_information`.`semester` = `teaches`.`semester`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_information_names`
--

/*!50001 DROP VIEW IF EXISTS `course_information_names`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_information_names` AS select `course_information`.`id` AS `id`,`course_information`.`course_name` AS `course_name`,`course_information`.`email` AS `email`,`course_information`.`school_year` AS `school_year`,`course_information`.`semester` AS `semester`,`course_information`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_information`.`id_course` AS `id_course`,`course_information`.`initial_hour` AS `initial_hour`,`course_information`.`final_hour` AS `final_hour`,`course_information`.`block_duration` AS `block_duration`,`course_information`.`block_for_unit` AS `block_for_unit`,`course_information`.`email_teach` AS `email_teach`,`course_information`.`id_unit` AS `id_unit`,`user`.`name` AS `name` from (`course_information` join `user` on((`course_information`.`email_teach` = `user`.`email`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_plan_teachers`
--

/*!50001 DROP VIEW IF EXISTS `course_plan_teachers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_plan_teachers` AS select `course_plan_information`.`id` AS `id`,`course_plan_information`.`course_name` AS `course_name`,`course_plan_information`.`email` AS `email`,`course_plan_information`.`school_year` AS `school_year`,`course_plan_information`.`semester` AS `semester`,`course_plan_information`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_plan_information`.`id_course` AS `id_course`,`course_plan_information`.`initial_hour` AS `initial_hour`,`course_plan_information`.`final_hour` AS `final_hour`,`course_plan_information`.`block_duration` AS `block_duration`,`course_plan_information`.`block_for_unit` AS `block_for_unit`,`exist`.`email_teacher` AS `email_teacher` from (`course_plan_information` join `exist` on(((`course_plan_information`.`id_course` = `exist`.`id_course`) and (`course_plan_information`.`school_year` = `exist`.`school_year`) and (`course_plan_information`.`semester` = `exist`.`semester`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `present_unitname`
--

/*!50001 DROP VIEW IF EXISTS `present_unitname`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `present_unitname` AS select `present`.`name` AS `name`,`present`.`school_year` AS `school_year`,`present`.`semester` AS `semester`,`present`.`id_course` AS `id_course`,`present`.`id_unit` AS `id_unit`,`course_unit`.`unit_name` AS `unit_name`,`course_unit`.`color` AS `color` from (`present` join `course_unit` on((`present`.`id_unit` = `course_unit`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_plan_classes`
--

/*!50001 DROP VIEW IF EXISTS `course_plan_classes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_plan_classes` AS select `course_plan_information`.`id` AS `id`,`course_plan_information`.`course_name` AS `course_name`,`course_plan_information`.`email` AS `email`,`course_plan_information`.`school_year` AS `school_year`,`course_plan_information`.`semester` AS `semester`,`course_plan_information`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_plan_information`.`id_course` AS `id_course`,`course_plan_information`.`initial_hour` AS `initial_hour`,`course_plan_information`.`final_hour` AS `final_hour`,`course_plan_information`.`block_duration` AS `block_duration`,`course_plan_information`.`block_for_unit` AS `block_for_unit`,`class`.`class_name` AS `class_name`,`class`.`class_type` AS `class_type`,`class`.`schedule` AS `schedule`,`class`.`schedule_url` AS `schedule_url` from (`course_plan_information` join `class` on(((`course_plan_information`.`id_course` = `class`.`id_course`) and (`course_plan_information`.`school_year` = `class`.`school_year`) and (`course_plan_information`.`semester` = `class`.`semester`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_plan_teachersnames`
--

/*!50001 DROP VIEW IF EXISTS `course_plan_teachersnames`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_plan_teachersnames` AS select `course_plan_teachers`.`id` AS `id`,`course_plan_teachers`.`course_name` AS `course_name`,`course_plan_teachers`.`email` AS `email`,`course_plan_teachers`.`school_year` AS `school_year`,`course_plan_teachers`.`semester` AS `semester`,`course_plan_teachers`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_plan_teachers`.`id_course` AS `id_course`,`course_plan_teachers`.`initial_hour` AS `initial_hour`,`course_plan_teachers`.`final_hour` AS `final_hour`,`course_plan_teachers`.`block_duration` AS `block_duration`,`course_plan_teachers`.`block_for_unit` AS `block_for_unit`,`course_plan_teachers`.`email_teacher` AS `email_teacher`,`teachers`.`name` AS `name` from (`course_plan_teachers` join `teachers` on((`course_plan_teachers`.`email_teacher` = `teachers`.`email`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_plan_units`
--

/*!50001 DROP VIEW IF EXISTS `course_plan_units`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_plan_units` AS select `course_plan_information`.`course_name` AS `course_name`,`course_plan_information`.`email` AS `email`,`course_plan_information`.`school_year` AS `school_year`,`course_plan_information`.`semester` AS `semester`,`course_plan_information`.`id_course` AS `id_course`,`course_unit`.`id` AS `id`,`course_unit`.`unit_name` AS `unit_name` from (`course_plan_information` join `course_unit` on(((`course_plan_information`.`id_course` = `course_unit`.`id_course`) and (`course_plan_information`.`school_year` = `course_unit`.`school_year`) and (`course_plan_information`.`semester` = `course_unit`.`semester`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `unitsnamestoclasses`
--

/*!50001 DROP VIEW IF EXISTS `unitsnamestoclasses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `unitsnamestoclasses` AS select `unitstoclasses`.`id` AS `id`,`unitstoclasses`.`course_name` AS `course_name`,`unitstoclasses`.`email` AS `email`,`unitstoclasses`.`school_year` AS `school_year`,`unitstoclasses`.`semester` AS `semester`,`unitstoclasses`.`unaval_stain_interval` AS `unaval_stain_interval`,`unitstoclasses`.`id_course` AS `id_course`,`unitstoclasses`.`initial_hour` AS `initial_hour`,`unitstoclasses`.`final_hour` AS `final_hour`,`unitstoclasses`.`block_duration` AS `block_duration`,`unitstoclasses`.`block_for_unit` AS `block_for_unit`,`unitstoclasses`.`class_name` AS `class_name`,`unitstoclasses`.`id_unit` AS `id_unit`,`course_unit`.`unit_name` AS `unit_name` from (`unitstoclasses` join `course_unit` on((`unitstoclasses`.`id_unit` = `course_unit`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_information_units`
--

/*!50001 DROP VIEW IF EXISTS `course_information_units`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_information_units` AS select `course_information`.`id` AS `id`,`course_information`.`course_name` AS `course_name`,`course_information`.`email` AS `email`,`course_information`.`school_year` AS `school_year`,`course_information`.`semester` AS `semester`,`course_information`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_information`.`id_course` AS `id_course`,`course_information`.`initial_hour` AS `initial_hour`,`course_information`.`final_hour` AS `final_hour`,`course_information`.`block_duration` AS `block_duration`,`course_information`.`block_for_unit` AS `block_for_unit`,`course_information`.`email_teach` AS `email_teach`,`course_information`.`id_unit` AS `id_unit`,`course_unit`.`unit_name` AS `unit_name` from (`course_information` join `course_unit` on((`course_information`.`id_unit` = `course_unit`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `teachers`
--

/*!50001 DROP VIEW IF EXISTS `teachers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `teachers` AS select `user`.`email` AS `email`,`user`.`password` AS `password`,`user`.`name` AS `name`,`user`.`number` AS `number` from (`user` join `teacher` on((`user`.`email` = `teacher`.`email`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `unitstoclasses`
--

/*!50001 DROP VIEW IF EXISTS `unitstoclasses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `unitstoclasses` AS select `course_information_classes`.`id` AS `id`,`course_information_classes`.`course_name` AS `course_name`,`course_information_classes`.`email` AS `email`,`course_information_classes`.`school_year` AS `school_year`,`course_information_classes`.`semester` AS `semester`,`course_information_classes`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_information_classes`.`id_course` AS `id_course`,`course_information_classes`.`initial_hour` AS `initial_hour`,`course_information_classes`.`final_hour` AS `final_hour`,`course_information_classes`.`block_duration` AS `block_duration`,`course_information_classes`.`block_for_unit` AS `block_for_unit`,`course_information_classes`.`class_name` AS `class_name`,`present`.`id_unit` AS `id_unit` from (`course_information_classes` join `present` on(((`course_information_classes`.`id_course` = `present`.`id_course`) and (`course_information_classes`.`school_year` = `present`.`school_year`) and (`course_information_classes`.`semester` = `present`.`semester`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `course_information_classes`
--

/*!50001 DROP VIEW IF EXISTS `course_information_classes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `course_information_classes` AS select `course_plan_information`.`id` AS `id`,`course_plan_information`.`course_name` AS `course_name`,`course_plan_information`.`email` AS `email`,`course_plan_information`.`school_year` AS `school_year`,`course_plan_information`.`semester` AS `semester`,`course_plan_information`.`unaval_stain_interval` AS `unaval_stain_interval`,`course_plan_information`.`id_course` AS `id_course`,`course_plan_information`.`initial_hour` AS `initial_hour`,`course_plan_information`.`final_hour` AS `final_hour`,`course_plan_information`.`block_duration` AS `block_duration`,`course_plan_information`.`block_for_unit` AS `block_for_unit`,`class`.`class_name` AS `class_name` from (`course_plan_information` join `class` on(((`course_plan_information`.`id_course` = `class`.`id_course`) and (`course_plan_information`.`school_year` = `class`.`school_year`) and (`course_plan_information`.`semester` = `class`.`semester`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-23  0:17:12
