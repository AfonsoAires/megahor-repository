-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: megahor
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course_unit`
--

DROP TABLE IF EXISTS `course_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course_unit` (
  `id` int NOT NULL,
  `unit_name` varchar(25) NOT NULL,
  `id_course` int NOT NULL,
  `school_year` varchar(25) NOT NULL,
  `semester` varchar(25) NOT NULL,
  `color` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_unit_course_plan` (`id_course`,`school_year`,`semester`),
  CONSTRAINT `fk_course_unit_course_plan` FOREIGN KEY (`id_course`, `school_year`, `semester`) REFERENCES `course_plan` (`id_course`, `school_year`, `semester`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_unit`
--

LOCK TABLES `course_unit` WRITE;
/*!40000 ALTER TABLE `course_unit` DISABLE KEYS */;
INSERT INTO `course_unit` VALUES (1,'DAM',0,'20/21','SV','#aab02f'),(2,'SA',0,'20/21','SV','#5a2883'),(3,'PCM',0,'20/21','SV','#49b39b'),(4,'MoP',0,'20/21','SV','#4b23f0'),(6,'DAM',0,'19/20','SI','#2a3af7'),(7,'IECD',0,'19/20','SI','#bd25d0'),(8,'SMI',0,'19/20','SI','#459a70'),(9,'MDP',0,'19/20','SV','#7b8e02'),(10,'IPM',0,'19/20','SI','#bfd888'),(11,'FSO',0,'19/20','SI','#1cea81'),(12,'EGP',0,'20/21','SI','#96e505');
/*!40000 ALTER TABLE `course_unit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-23  0:17:11
