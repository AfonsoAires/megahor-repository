-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: megahor
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course_plan`
--

DROP TABLE IF EXISTS `course_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course_plan` (
  `school_year` varchar(75) NOT NULL,
  `semester` varchar(25) NOT NULL,
  `unaval_stain_interval` varchar(75) NOT NULL,
  `id_course` int NOT NULL,
  `initial_hour` varchar(25) DEFAULT NULL,
  `final_hour` varchar(25) DEFAULT NULL,
  `block_duration` varchar(25) DEFAULT NULL,
  `block_for_unit` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`school_year`,`semester`,`id_course`),
  KEY `fk_course_plan` (`id_course`),
  CONSTRAINT `fk_course_plan` FOREIGN KEY (`id_course`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_plan`
--

LOCK TABLES `course_plan` WRITE;
/*!40000 ALTER TABLE `course_plan` DISABLE KEYS */;
INSERT INTO `course_plan` VALUES ('19/20','SI','1/1/1 - 1/1/2',0,'09:30','14:00','01:30','3'),('19/20','SV','1/1/1 - 1/1/2',0,'12:30','18:30','01:30','4'),('20/21','SI','1/1/1 - 1/1/2',0,NULL,NULL,NULL,NULL),('20/21','SV','1/1/1 - 1/1/2',0,'09:30','18:30','01:30','3');
/*!40000 ALTER TABLE `course_plan` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-23  0:17:10
