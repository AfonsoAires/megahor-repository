-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: megahor
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `teaches`
--

DROP TABLE IF EXISTS `teaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teaches` (
  `email_teach` varchar(75) NOT NULL,
  `school_year` varchar(25) NOT NULL,
  `semester` varchar(25) NOT NULL,
  `id_course` int NOT NULL,
  `id_unit` int NOT NULL,
  PRIMARY KEY (`email_teach`,`id_course`,`school_year`,`semester`,`id_unit`),
  KEY `fk_teaches_course_plan` (`id_course`,`school_year`,`semester`),
  KEY `fk_teaches_course_unit` (`id_unit`),
  CONSTRAINT `fk_teaches_course_plan` FOREIGN KEY (`id_course`, `school_year`, `semester`) REFERENCES `course_plan` (`id_course`, `school_year`, `semester`),
  CONSTRAINT `fk_teaches_course_unit` FOREIGN KEY (`id_unit`) REFERENCES `course_unit` (`id`),
  CONSTRAINT `fk_teaches_teacher` FOREIGN KEY (`email_teach`) REFERENCES `teacher` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaches`
--

LOCK TABLES `teaches` WRITE;
/*!40000 ALTER TABLE `teaches` DISABLE KEYS */;
INSERT INTO `teaches` VALUES ('antonio@gmail.com','20/21','SV',0,1),('pedrof@gmail.com','20/21','SV',0,1),('manfred@gmail.com','20/21','SV',0,2),('rui@gmail.com','20/21','SV',0,3),('antonio@gmail.com','20/21','SV',0,4),('pedrof@gmail.com','20/21','SV',0,4),('antonio@gmail.com','19/20','SI',0,6),('porfirio@gmail.com','19/20','SI',0,7),('cajo@gmail.com','19/20','SI',0,8),('antonio@gmail.com','19/20','SV',0,9),('rui@gmail.com','19/20','SI',0,10),('cajo@gmail.com','19/20','SI',0,11),('jp@gmail.com','19/20','SI',0,11);
/*!40000 ALTER TABLE `teaches` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-23  0:17:10
