//##################### COURSE MANAGEMENT #################################


//%%%%%%%%%%%%%%%%%%%%%% Tree view %%%%%%%%%%%%%%%%%%%%%%
//Set clicks to treeview
function setClicks(){
    let toggler = document.getElementsByClassName("caret");
    let i;
    //Toggle nested between active and nothing
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }

}

//Add teachers, units or classes with/without prompt
function addToList(plus, needPrompt, name, id){
    let xhttp;

    //List above plus button
    let List = plus.parentElement;

    //Pegar no path para meter no id
    let str = plus.id;
    let splitted = str.split("-");
    let course = splitted[0];
    let school_year = splitted[1];
    let semester = splitted[2];
    let entity = splitted[3];
    let create = false;

    //If it was the coordinator that create the teacher
    if(needPrompt){
        let nameTextPrompt = prompt("Insert the name of the entity", "");
        name = nameTextPrompt; //Name its coordinator input
        id = name;
        create = true;
    }
    if (name != null) {
        if(create){ //if was created by user
            if(entity === 'plusTeacher'){ //if was a unit
                str = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + id; 

                //Create each element of the list with the correspondent name
                let li = document.createElement("li"); //li element of list
                li.setAttribute("onclick", "Teacher(this)");//On click to display content page
                li.setAttribute("style", "cursor: pointer;");
                li.setAttribute("id", str);//On click to display content page
                let newName = document.createTextNode(name);//name desired
                li.appendChild(newName); //add the new name in the list node
                List.insertBefore(li,  plus);//add the new list node before plus

            }else if(entity === 'plusUnit'){ //if was a unit

                //Pick a random color
                let hexColor = Math.floor(Math.random()*16777215).toString(16);

                //Pass to rgb and check if it is too dark
                let c = hexColor.substring(1); // strip #
                let rgb = parseInt(c, 16);   // convert rrggbb to decimal
                let r = (rgb >> 16) & 0xff;  // extract red
                let g = (rgb >>  8) & 0xff;  // extract green
                let b = (rgb >>  0) & 0xff;  // extract blue
                let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

                //If it is too dark, random color again
                while (luma < 40) {
                    hexColor = Math.floor(Math.random()*16777215).toString(16);
                    c = hexColor.substring(1);      // strip #
                    rgb = parseInt(c, 16);   // convert rrggbb to decimal
                    r = (rgb >> 16) & 0xff;  // extract red
                    g = (rgb >>  8) & 0xff;  // extract green
                    b = (rgb >>  0) & 0xff;  // extract blue

                    luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
                }


                //CHECK UNIT ID
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) { //Check max id to put next id
                    result = JSON.parse(this.responseText).result;
                    id = result[0]["MAX(ID)"] + 1;
                    str = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + id; 

                    xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) { //Put the unit in the BD wit that ID
                            //Criar list element
                            let li = document.createElement("li"); //li element of list
                            li.setAttribute("onclick", "Teacher(this)");//On click to display content page
                            li.setAttribute("style", "cursor: pointer;");
                            li.setAttribute("id", str);
                            let newName = document.createTextNode(name);//name desired
                            li.appendChild(newName); //add the new name in the list node

            
                            List.insertBefore(li,  plus);//add the new list node before plus
                        }
                    };
                    xhttp.open("POST", "createUnit", true);
                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&name=" + name + "&id=" + id + "&color=#" + hexColor);


                    }
                };
                xhttp.open("GET", "checkUnitID", true);
                xhttp.send();
            }else if(entity === 'plusClass'){ //if it is a class
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) { //Put class in BD
                        str = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + id; 

                        //Criar elemento da lista com nome do elemento
                        let li = document.createElement("li"); //li element of list
                        li.setAttribute("onclick", "Teacher(this)");//On click to display content page
                        li.setAttribute("style", "cursor: pointer;");
                        li.setAttribute("id", str);
                        let newName = document.createTextNode(name);//name desired
                        li.appendChild(newName); //add the new name in the list node
                
                        List.insertBefore(li,  plus);//add the new list node before plus
                    }
                };
                xhttp.open("POST", "createClass", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&name=" + name);

            }
        }else{ //If was not created by the user it was automatically and just want to put in tree
            str = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + id; 

            //Create element in list
            let li = document.createElement("li"); //li element of list
            li.setAttribute("onclick", "Teacher(this)");//On click to display content page
            li.setAttribute("style", "cursor: pointer;");
            li.setAttribute("id", str);//On click to display content page
            let newName = document.createTextNode(name);//name desired
            li.appendChild(newName); //add the new name in the list node
    
            List.insertBefore(li,  plus);//add the new list node before plus
        }
    }
}



//Function that chooses which content appears accordingly where the user clicks in tree view
function Teacher(subject){
    document.getElementById("introdution").hidden=true;//Hide introduction title of course
    document.getElementById("TITLE").textContent = subject.innerText;//Display name of entity as title

    let current;

    //Traverse all entities and choose the correct info
    let lists = ["teacherList", "courseUnitsList", "classesList"];
    let List;
    for (let l = 0; l < lists.length; l++) {
        List = document.getElementsByClassName(lists[l]);
        for (let y = 0; y < List.length; y++) {
            for (let t = 0; t < List[y].children.length; t++) {
                if(List[y].children[t].textContent === subject.textContent){
                    if(l === 0){
                        current = "teacherList";
                        document.getElementById("associateEmail").hidden=false;
                        document.getElementById("associateTeacher").hidden=true;
                        document.getElementById("associateCourseUnits").hidden=true;
                        

                    }else if(l === 1){
                        current = "courseUnitsList";
                        document.getElementById("associateEmail").hidden=true;
                        document.getElementById("associateTeacher").hidden=false;
                        document.getElementById("associateCourseUnits").hidden=true;

                    }else{
                        current = "classesList";
                        document.getElementById("associateEmail").hidden=true;
                        document.getElementById("associateTeacher").hidden=true;
                        document.getElementById("associateCourseUnits").hidden=false;
                    }
                }
            }
        }
    }

    //Each entity have an id = "course - school year - semester - id"
    let splitted = subject.id.split("-");
    let course = splitted[0];
    let school_year = splitted[1];
    let semester = splitted[2];
    let name = splitted[3];


    let xhttp;
    switch (current) {
        //get teacher stain from db to show
        case 'teacherList': 
            document.getElementById('email').value = name;
            document.getElementById('teacherName').value = subject.textContent;
            document.getElementById('teacherID').value = subject.id;
            fill_teacherMails(course, school_year, semester);

            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    result = JSON.parse(this.responseText).result;
                    console.log(result);

                    if(result.length>0){
                        document.getElementById('teacherSchedule').value = result[0].schedule;
                        document.getElementById("teacherStainImage").src = result[0].schedule_url;
                        document.getElementById("teacherStainAlert").innerHTML = ""; 
                        document.getElementById("createEditTeacherStain").innerHTML = "Edit Stain"; 
                        document.getElementById("notifyTeacher").hidden = true; 

                    }else{
                        document.getElementById('teacherSchedule').value = "";
                        document.getElementById("teacherStainImage").src = "";
                        document.getElementById("teacherStainAlert").innerHTML = "This teacher doesn't have a unavailability stain yeat!"; 
                        document.getElementById("createEditTeacherStain").innerHTML = "Create Stain"; 
                        document.getElementById("notifyTeacher").hidden = false; 
                        
                    }



                }
            };
            xhttp.open("GET", "getTeacherStainToManage?name=" + name, true);
            xhttp.send();

            break;
        case 'courseUnitsList': 
            document.getElementById("contextUnits").value = subject.id;
            //AJAX request for teachers for that unit
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    result = JSON.parse(this.responseText).result;

                    //remove all childs of teachers list
                    let ul = document.getElementById("unitsTeacher");
                    while( ul.firstChild ){
                        ul.removeChild( ul.firstChild );
                    }
                    //Create all teacher that are linked to that unit
                    for (let k = 0; k < result.length; k++) {
                        let li = document.createElement("li");
                        let name_teach = document.createTextNode(result[k].name + "     ");
                        li.appendChild(name_teach);
                        let a = document.createElement("a");
                        a.setAttribute("style","text-decoration:underline; color:red; cursor: pointer;");
                        a.setAttribute("onclick","deleteAssociations(this,'courseUnitsList','" + course + "-" + school_year + "-" + semester + "-" + name + "-" + result[k].email_teach + "')");
                        a.innerHTML = "Eliminate";
                        li.appendChild(a);
                        ul.appendChild(li);
                    }
                    fill_teachers(course, school_year, semester);
                }
            };
            xhttp.open("GET", "setTeacherListToUnits?course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&name=" + name, true);
            xhttp.send();
            
            break;
        case 'classesList':
            document.getElementById("contextClasses").value = subject.id;
            document.getElementById("classNameForSchedule").value = subject.id;
            //AJAX request for units for that class
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    result = JSON.parse(this.responseText).result;

                    let teachers = JSON.parse(this.responseText).teachers;

                    //remove all childs of teachers list
                    let ul = document.getElementById("classUnits");
                    while( ul.firstChild ){
                        ul.removeChild( ul.firstChild );
                    }
                    //Put all units with the correspondent teacher ahead
                    for (let k = 0; k < result.length; k++) {
                        let str = result[k].unit_name + "-" + result[k].id_unit + " (";
                        for (let y = 0; y < teachers[k].length; y++) {
                            str += teachers[k][y] + "; ";
                        }
                        str += ") "

                        let li = document.createElement("li");
                        li.appendChild(document.createTextNode(str));
                        let a = document.createElement("a");
                        a.setAttribute("cursor","cursor: pointer;");
                        a.setAttribute("style","text-decoration:underline; color:red; cursor: pointer;");
                        a.setAttribute("onclick","deleteAssociations(this,'classesList','" + course + "-" + school_year + "-" + semester + "-" + name + "-" + result[k].id_unit + "')");
                        a.innerHTML = " Eliminate";
                        li.appendChild(a);
                        ul.appendChild(li);
                    }
                    fill_units(course, school_year, semester);

                    //Class type definitions
                    let class_type = JSON.parse(this.responseText).class_type;
                    console.log("class_type");
                    console.log(class_type);

                    //Choose between day class or night class in select element
                    if(class_type[0].class_type === "day_class"){
                        document.getElementById("class_select").selectedIndex = "0";
                    }else{
                        document.getElementById("class_select").selectedIndex = "1";
                    }
                    document.getElementById("class_type").value = class_type[0].class_type;

                    //Choose between button value = create or edit & put schedule image in html & schedule string to the input type hidden for the next page
                    if(class_type[0].schedule_url === ""){
                        console.log("nao tem horario");
                        document.getElementById("createClassScheduleBtn").value = "Create Class Schedule";
                        document.getElementById("class_schedule").value = "";                    
                        document.getElementById("classScheduleImage").src = "";   
                    }else{
                        console.log("tem horario");
                        document.getElementById("createClassScheduleBtn").value = "Edit Class Schedule";
                        document.getElementById("class_schedule").value = class_type[0].schedule;                    
                        document.getElementById("classScheduleImage").src = class_type[0].schedule_url;   
                    }

                    let definitions = JSON.parse(this.responseText).definitions;

                    //If there are definitions put the correct hours, if not say that there are none.
                    if(definitions.length > 0){
                        if(class_type[0].class_type === "night_class"){
                            document.getElementById("definitions").innerHTML = "18:30-23:00 (01:30)";
                        }else{
                            document.getElementById("definitions").innerHTML = definitions[0] + "-" + definitions[1] + " (" + definitions[2] + ")";
                        }
                    }else{
                        document.getElementById("definitions").innerHTML = "No hours settings yet";
                    }

                 

                }
            };
            xhttp.open("GET", "setUnitsListToClasses?course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&name=" + name, true);
            xhttp.send();
            //Fill dropdown
          break;
      }
}

//Check if the current class have the hours definitions settings (called in submit of form)
function checkIfDefinitions(){
    let definitions = document.getElementById("definitions").innerHTML;
    if(definitions === "No hours settings yet"){
        alert("No hours definitions settings. Please set some hours definitions!");
        return false;
    }else{
        return true;
    }
}



//Add content in treeview root 
function addToTreeView(courseID, course, schoolYear, semester){

    let str = '<li><span class="caret">' + course + '</span>'
                + '<ul class="nested">';

                for (let i = 0; i < schoolYear.length; i++) {
                    str += '<li><span class="caret">' + schoolYear[i] + '</span>'
                    + '<ul class="nested">';


                    for (let j = 0; j < semester.length; j++) {

                        let idTeacher = courseID + "-" + schoolYear[i] + "-" + semester[j] + "-plusTeacher";
                        let idUnit = courseID + "-" + schoolYear[i] + "-" + semester[j] + "-plusUnit";
                        let idClass = courseID + "-" + schoolYear[i] + "-" + semester[j] + "-plusClass";

                        str +=      '<li><span class="caret">' + semester[j] + '</span>'
                        +            '<ul class="nested">'
                        +            '<li><span class="caret">Teacher</span>'
                        +                '<ul class="nested teacherList">'
                        +                    '<li class="row ml-2" style="cursor: pointer;" id="' + idTeacher + '" onclick="addToList(this, true, \'\', \'\')">'
                        +                        '<img src="plus.png" width="20" height="20" alt="">'
                        +                    '</li>'
                        +                '</ul>'
                        +            '</li>'
                        +            '<li><span class="caret">Course Units</span>'
                        +                '<ul class="nested courseUnitsList">'
                        +                    '<li class="row ml-2" style="cursor: pointer;" id="' + idUnit + '" onclick="addToList(this, true, \'\', \'\')">'
                        +                    '<img src="plus.png" width="20" height="20" alt="">'
                        +                '</li>'
                        +                '</ul>'
                        +            '</li>'
                        +            '<li><span class="caret">Classes</span>'
                        +                '<ul class="nested classesList" id="classesList">'
                        +                    '<li class="row ml-2" style="cursor: pointer;" id="' + idClass + '" onclick="addToList(this, true, \'\', \'\')">'
                        +                    '<img src="plus.png" width="20" height="20" alt="">'
                        +                '</li>'
                        +                '</ul>'
                        +            '</li>'
                        +            '</ul>'
                        +        '</li>';
                    }

                   
                    str += '</ul>'
                    +  '</li>';
                }

        str +=    '</ul>'
                +'</li>';

    document.getElementById('treeView').insertAdjacentHTML( 'beforeend', str );
}

//set List of teachers, units and classes with the correct course, school year and semester
function setLists(){
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) { 


        //#####################################
        //Set all courses and course plans
        let courses = JSON.parse(this.responseText).courses;
        ////console.log(courses);

        //Group by ID
        courses, result = courses.reduce(function (r, a) {
                r[a.id] = r[a.id] || [];
                r[a.id].push(a);
                return r;
            }, Object.create(null));
        //Convert to array
        let new_result = Object.entries(result);

        //Convert to a proper array
        let arrayID = [];
        for (let i = 0; i < new_result.length; i++) {
            arrayID.push(new_result[i][1]);
        }
        ////console.log(arrayID);

        //Check which school year has each course
        let arraySchoolYear = [];
        for (let i = 0; i < arrayID.length; i++) {
            let aux = [];
            for (let j = 0; j < arrayID[i].length; j++) {
                aux.push(arrayID[i][j].school_year);
            }
            //Remove duplicates
            let uniqueAux = deleteDuplicates(aux);
            arraySchoolYear.push(uniqueAux);
        }

        ////console.log(arrayID);
        ////console.log(arraySchoolYear);


        let array2 = ["SI","SV"];
        //Finnaly put all courses with correspondent school year
        for (let i = 0; i < arrayID.length; i++) {
            ////console.log(arrayID[i][0].course_name);
            addToTreeView(arrayID[i][0].id, arrayID[i][0].course_name, arraySchoolYear[i], array2);
        }

        setClicks();



        //#####################################
        //Set teachers names
        let teachers = JSON.parse(this.responseText).teachers;
        //////console.log(teachers);
        let all = [];
        for (let i = 0; i < teachers.length; i++) {
            let course_name = teachers[i].id;
            let school_year = teachers[i].school_year;
            let semester = teachers[i].semester;
            let teacher_name = teachers[i].name;
            let teacher_email = teachers[i].email_teacher;
            let str = course_name + "-" + school_year + "-" + semester + "-" + "plusTeacher#" + teacher_name + "#" + teacher_email;
            all.push(str);
        }


        //////console.log("all - " + all);

        //Remove duplicates
        let uniqueAll = deleteDuplicates(all);

        ////console.log("all removed - " + uniqueAll);

        for (let i = 0; i < uniqueAll.length; i++) {
            //////console.log(uniqueAll[i]);
            let splitted = uniqueAll[i].split("#");
            addToList(document.getElementById(splitted[0]), false, splitted[1], splitted[2]);
        }




        //#####################################
        //Set units names
        let units = JSON.parse(this.responseText).units;
        //////console.log(units);
        all = [];
        for (let i = 0; i < units.length; i++) {
            let course_name = units[i].id_course;
            let school_year = units[i].school_year;
            let semester = units[i].semester;
            let unit_name = units[i].unit_name;
            let id_unit = units[i].id;
            let str = course_name + "-" + school_year + "-" + semester + "-" + "plusUnit#" + unit_name + "#" + id_unit;
            all.push(str);
        }

        //console.log("all - " + all);

        //Remove duplicates
        uniqueAll = deleteDuplicates(all);

       //////console.log("all removed - " + uniqueAll);

        for (let i = 0; i < uniqueAll.length; i++) {
            //////console.log(uniqueAll[i]);
            let splitted = uniqueAll[i].split("#");
            addToList(document.getElementById(splitted[0]), false, splitted[1], splitted[2]);
        }



        //#####################################
        //Set classes names
        let classes = JSON.parse(this.responseText).classes;
        //console.log(classes);
        all = [];
        for (let i = 0; i < classes.length; i++) {
            let course_name = classes[i].id;
            let school_year = classes[i].school_year;
            let semester = classes[i].semester;
            let class_name = classes[i].class_name;
            let str = course_name + "-" + school_year + "-" + semester + "-" + "plusClass#" + class_name + "#" + class_name;
            all.push(str);


        }

        //////console.log("all - " + all);

        //Remove duplicates
        uniqueAll = deleteDuplicates(all);

        ////console.log("all removed - " + uniqueAll);

        for (let i = 0; i < uniqueAll.length; i++) {
            ////console.log(uniqueAll[i]);
            let splitted = uniqueAll[i].split("#");
            addToList(document.getElementById(splitted[0]), false, splitted[1], splitted[2]);
        }

      }
    };
    xhttp.open("GET", "setLists", true);
    xhttp.send();
}


//Associate teacher to unit
function associateTeacher(){

    let context = document.getElementById("contextUnits").value; //Unit course, school year, semester and id
    let teacher = document.getElementById("teacher").value; //Teacher info name-email

    let teacherName = teacher.split("-")[0];
    let teacherEmail = teacher.split("-")[1];

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200){
            //Check if that teacher already exists
            let chidlren = document.getElementById("unitsTeacher").children;
            let exist = false;

            for (let i = 0; i < chidlren.length; i++) {
                let teacherNameList = chidlren[i].innerText.split(" ")[0];
                if(teacherName === teacherNameList){
                    exist = true;
                }
              }

            // if he doesnt exists add to list
            if(!exist){
                 let ul = document.getElementById("unitsTeacher");
                 let li = document.createElement("li");
                 let name_teach = document.createTextNode(teacherName + "     ");
                 li.appendChild(name_teach);
                 let a = document.createElement("a");
                 a.setAttribute("style","text-decoration:underline; color:red; cursor: pointer;");
                 a.setAttribute("onclick","deleteAssociations(this,'courseUnitsList','" + context + "-" + teacherEmail + "')");
                 a.innerHTML = "Eliminate";
                 li.appendChild(a);
                 ul.appendChild(li);
            }
            
        }
    };
    xhttp.open("POST", "unitTeachers_submit", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("teacher=" + teacher + "&contextUnits=" + context);

}

//Associate unit to class
function associateUnit(){
    let contextClasses = document.getElementById("contextClasses").value; //Context of entity, course, school year, semester, id
    let unit = document.getElementById("unit").value; //name-id

    let unitName = unit.split("-")[0];
    let unitID = unit.split("-")[1];

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200){
            let chidlren = document.getElementById("classUnits").children;
            let exist = false;

            //Check if that units already exists
            for (let i = 0; i < chidlren.length; i++) {
                let unitNameList = chidlren[i].innerText.split(" ")[0];
                if(unit === unitNameList){
                    exist = true;
                }
              }

            if(!exist){
                 let ul = document.getElementById("classUnits");
                 let li = document.createElement("li");
                 let name_teach = document.createTextNode(unit + "     ");
                 li.appendChild(name_teach);
                 let a = document.createElement("a");
                 a.setAttribute("style","text-decoration:underline; color:red; cursor: pointer;");
                 a.setAttribute("onclick","deleteAssociations(this,'classesList','" + contextClasses + "-" + unitID + "')");
                 a.innerHTML = "Eliminate";
                 li.appendChild(a);
                 ul.appendChild(li);
            }

        }
    };
    xhttp.open("POST", "classesUnits_submit", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("contextClasses=" + contextClasses + "&unit=" + unit);
}



//%%%%%%%%%%%%%%%%%%%%%% Logic in entities relations %%%%%%%%%%%%%%%%%%%%%%

//fill dropdown list with email to choose in teachers
function teacherMails_submit(){
    let teacherName = document.getElementById("teacherName").value;
    let teacherID = document.getElementById("teacherID").value;
    let email = document.getElementById("email").value;
    let splitted = teacherID.split("-");


    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4){
            let nome = JSON.parse(this.responseText).nome;
            //If that teacher already existe in that context
            if(nome === "erro"){
                alert("Sorry, that teacher already exists in school year " + splitted[1] + " and semester " + splitted[2]);
                document.getElementById("email").focus();
            //If teacher doesnt exists in bd
            }else if (nome === "criado") {
                alert("Teacher created and added to school year " + splitted[1] + " and semester " + splitted[2]);
                document.getElementById("email").value = email;
                document.getElementById("teacherID").value = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + email;
                let idInTreeView = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + teacherName;
                document.getElementById(idInTreeView).id = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + email;
            }else{
                //If teacher exists in bd but doesnt exist in that context
                alert("Teacher added to school year " + splitted[1] + " and semester " + splitted[2]);
                document.getElementById("TITLE").innerHTML = nome;
                document.getElementById(teacherID).innerHTML = nome;
                document.getElementById(teacherID).id = splitted[0] + "-" + splitted[1] + "-" + splitted[2] + "-" + email + "-";
            } 
        }

    };
    xhttp.open("POST", "teacherMails_submit", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("teacherName=" + teacherName + "&teacherID=" + teacherID + "&email=" + email);
}

//fill dropdown list with email to choose in teachers
function fill_teacherMails(course, school_year, semester){
    let teacherEmailListDB = [];
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        
        teacherEmailListDB = JSON.parse(this.responseText).teacherEmails;

        //console.log(teacherEmailListDB);
        
        let x = document.getElementById("emailDroplist");
        //Remove previous options
        x.innerText = null;

        for (let i = 0; i < teacherEmailListDB.length; i++) {
            let new_option = document.createElement("option");
            //console.log(teacherEmailListDB[i]);
            new_option.value = teacherEmailListDB[i];
            //console.log(new_option.value);
            x.appendChild(new_option); 
        }
        //document.getElementById(param).onclick = ()=> false;
      }
    };
    xhttp.open("GET", "teachers?course=" + course + "&school_year=" + school_year + "&semester=" + semester, true);
    xhttp.send();
}

//fill dropdown list with teacher names to choose in units
function fill_teachers(course, school_year, semester){
    let teacherEmailListDB = [];
    let teacherListDB = [];
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        
        teacherEmailListDB = JSON.parse(this.responseText).teacherEmails;
        teacherListDB = JSON.parse(this.responseText).teacherNames;

        console.log(teacherEmailListDB);
        console.log(teacherListDB);
        
        let x = document.getElementById("teacherDroplist");
        //Remove previous options
        x.innerText = null;

        for (let i = 0; i < teacherListDB.length; i++) {
            let new_option = document.createElement("option");
            //console.log(teacherListDB[i]);
            new_option.value = teacherListDB[i] + "-" + teacherEmailListDB[i];
            //console.log(new_option.value);
            x.appendChild(new_option); 
        }
        //document.getElementById(param).onclick = ()=> false;
      }
    };
    xhttp.open("GET", "teachersFromUnit?course=" + course + "&school_year=" + school_year + "&semester=" + semester, true);
    xhttp.send();
}

//fill dropdown list with units to choose in classes
function fill_units(course, school_year, semester){
    let unitsListDB = [];
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        
        unitsListDB = JSON.parse(this.responseText).units;
        console.log(unitsListDB);
        let x = document.getElementById("unitDroplist");

        //Remove previous options
        x.innerText = null;

        //Remove ids duplicates
        let aux_id = [];
        for (let i = 0; i < unitsListDB.length; i++) {
            aux_id.push(unitsListDB[i].id);
            }
        let uniqueAux_id = deleteDuplicates(aux_id);

        //Remove names duplicates
        let aux_name = [];
        for (let i = 0; i < unitsListDB.length; i++) {
            aux_name.push(unitsListDB[i].unit_name);
            }
        let uniqueAux_name = deleteDuplicates(aux_name);

        //Insert in datalist
        for (let i = 0; i < uniqueAux_name.length; i++) {
            let new_option = document.createElement("option");
            ////console.log(uniqueAux_name[i]);
            new_option.value = uniqueAux_name[i] + "-" + uniqueAux_id[i];
            //new_option.innerText = uniqueAux_id[i];
            //new_option.setAttribute("data-value", uniqueAux_id[i]);
            //console.log(new_option.value);
            x.appendChild(new_option); 
        }
        //document.getElementById(param).onclick = ()=> false;
      }
    };
    xhttp.open("GET", "units?course=" + course + "&school_year=" + school_year + "&semester=" + semester, true);
    xhttp.send();
}


//delete a certain entity
function deleteEntity(t){
    if (confirm('Are you sure you want to delete this entity?')) {

        let context, splitted, course, school_year, semester, entity, xhttp, treeElement;
        switch(t.id) {
            //Delete from teaches and exists
            case "deleteTeacher":
                //check context of teacher to eliminate
                context = document.getElementById("teacherID").value;
                splitted = context.split("-");
                course = splitted[0];
                school_year = splitted[1];
                semester = splitted[2];
                entity = splitted[3];
    
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) { 
                        //Remove from HTML
                        treeElement = document.getElementById(context);
                        treeElement.parentElement.removeChild(treeElement);
    
                        document.getElementById("TITLE").textContent = "Teacher removed sucessfully!";//Display name as title
                        document.getElementById("associateEmail").hidden=true;
                        document.getElementById("associateTeacher").hidden=true;
                        document.getElementById("associateCourseUnits").hidden=true;
                    }
                };
                xhttp.open("POST", "deleteTeacher", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&entity=" + entity);
                break;

            //Delete from teaches, resides and present specific classes of course plan
            case "deleteUnit":
                //check context of unit to eliminate
                context = document.getElementById("contextUnits").value;
                splitted = context.split("-");
                course = splitted[0];
                school_year = splitted[1];
                semester = splitted[2];
                entity = splitted[3];
    
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) { 
                        //Remove from HTML
                        treeElement = document.getElementById(context);
                        treeElement.parentElement.removeChild(treeElement);
    
                        document.getElementById("TITLE").textContent = "Unit removed sucessfully!";//Display name as title
                        document.getElementById("associateEmail").hidden=true;
                        document.getElementById("associateTeacher").hidden=true;
                        document.getElementById("associateCourseUnits").hidden=true;
                    }
                };
                xhttp.open("POST", "deleteUnit", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&entity=" + entity);
                break;

            //Delete from that course plan
            case "deleteClass":
                //check context of class to eliminate
                context = document.getElementById("contextClasses").value;
                splitted = context.split("-");
                course = splitted[0];
                school_year = splitted[1];
                semester = splitted[2];
                entity = splitted[3];
    
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) { 
                        //Remove from HTML
                        console.log("removed");
                        treeElement = document.getElementById(context);
                        treeElement.parentElement.removeChild(treeElement);
    
                        document.getElementById("TITLE").textContent = "Class removed sucessfully!";//Display name as title
                        document.getElementById("associateEmail").hidden=true;
                        document.getElementById("associateTeacher").hidden=true;
                        document.getElementById("associateCourseUnits").hidden=true;
                    }
                };
                xhttp.open("POST", "deleteClass", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&entity=" + entity);
    
            break;
    
          }
      } 
}

//delete an association
function deleteAssociations(button, where, what){
    let splitted, course, school_year, semester, fromEliminate, toElimate, xhttp;

    //where to eliminate and what
    if(where === "courseUnitsList"){
        splitted = what.split("-");
        course = splitted[0];
        school_year = splitted[1];
        semester = splitted[2];
        fromEliminate = splitted[3];
        toElimate = splitted[4];

    
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) { 
                //Remove from HTML by button clicked, previous element
                console.log("removed");
                let ul = document.getElementById("unitsTeacher");
                ul.removeChild(button.parentElement);
            }
        };
        xhttp.open("POST", "deleteTeacherInsideUnit", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&fromEliminate=" + fromEliminate + "&toElimate=" + toElimate);    
    }else if(where === "classesList"){
        splitted = what.split("-");
        course = splitted[0];
        school_year = splitted[1];
        semester = splitted[2];
        fromEliminate = splitted[3];
        toElimate = splitted[4];

    
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) { 
                //Remove from HTML
                console.log("removed");
                let ul = document.getElementById("classUnits");
                ul.removeChild(button.parentElement);
            }
        };
        xhttp.open("POST", "deleteUnitInsideClass", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&fromEliminate=" + fromEliminate + "&toElimate=" + toElimate);    
    }

    


}

//set all course plans for course definitions
function setCoursePlans(){
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        
        let coursePlanInfo = JSON.parse(this.responseText).courses;

        let ul = document.getElementById("coursePlansUL");

        for (let i = 0; i < coursePlanInfo.length; i++) {
            let id = coursePlanInfo[i].id_course + "-" + coursePlanInfo[i].school_year + "-" + coursePlanInfo[i].semester;

            let initial_hour_value = coursePlanInfo[i].initial_hour;
            if(initial_hour_value === null)
                initial_hour_value = "";
            let final_hour_value = coursePlanInfo[i].final_hour;
            if(final_hour_value === null)
                final_hour_value = "";
            let block_duration_value = coursePlanInfo[i].block_duration;
            if(block_duration_value === null)
                block_duration_value = "";
            let block_for_unit_value = coursePlanInfo[i].block_for_unit;
            if(block_for_unit_value === null)
                block_for_unit_value = "";


            //Create form
            let form = document.createElement("form");
            form.setAttribute("class", "my-3");
            form.setAttribute("action", "/updateCoursePlanInfo");
            form.setAttribute("method", "POST");
            ul.appendChild(form);

            //Create title of each course and course plan
            let li = document.createElement("h5");
            li.innerHTML = coursePlanInfo[i].course_name + " - " + coursePlanInfo[i].school_year + " - " + coursePlanInfo[i].semester;
            form.appendChild(li);

            //Create input type hidden with id
            let input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("id", "context");
            input.setAttribute("name", "context");
            input.setAttribute("value", id);
            form.appendChild(input);



            //Create div
            let div1 = document.createElement("div");
            div1.setAttribute("class", "row ml-5 my-2");
            form.appendChild(div1);


            //Create paragraph for initail hour
            let initial_hour = document.createElement("label");
            initial_hour.setAttribute("for", id + "-initial_hour");
            initial_hour.setAttribute("class", "mr-2");
            initial_hour.innerHTML = 'Initial hour: ';
            div1.appendChild(initial_hour);
            //Create input for initail hour
            input = document.createElement("input");
            input.setAttribute("type", "time");
            input.setAttribute("min", "08:00");
            input.setAttribute("max", "23:00");
            input.setAttribute("id", id + "-initial_hour");
            input.setAttribute("name", "initial_hour");
            input.setAttribute("value", initial_hour_value);
            div1.appendChild(input);



            //Create div
            let div2 = document.createElement("div");
            div2.setAttribute("class", "row ml-5 my-2");
            form.appendChild(div2);

            //Create paragraph for final hour
            let final_hour = document.createElement("label");
            final_hour.setAttribute("for", id + "-final_hour");
            final_hour.setAttribute("class", "mr-2");
            final_hour.innerHTML = 'Final hour: ';
            div2.appendChild(final_hour);
            //Create input for final hour
            input = document.createElement("input");
            input.setAttribute("type", "time");
            input.setAttribute("min", "08:00");
            input.setAttribute("max", "23:00");
            input.setAttribute("id", id + "-final_hour");
            input.setAttribute("name", "final_hour");
            input.setAttribute("value", final_hour_value);
            div2.appendChild(input);


            //Create div
            let div3 = document.createElement("div");
            div3.setAttribute("class", "row ml-5 my-2");
            form.appendChild(div3);

            //Create paragraph for block duration
            let block_duration = document.createElement("label");
            block_duration.setAttribute("for", id + "-block_duration");
            block_duration.setAttribute("class", "mr-2");
            block_duration.innerHTML = 'Block duration: ';
            div3.appendChild(block_duration);
            //Create input for block duration
            input = document.createElement("input");
            input.setAttribute("type", "time");
            input.setAttribute("min", "00:30");
            input.setAttribute("max", "6:00");
            title="Three letter country code"
            input.setAttribute("id", id + "-block_duration");
            input.setAttribute("name", "block_duration");
            input.setAttribute("value", block_duration_value);
            div3.appendChild(input);


            //Create div
            let div4 = document.createElement("div");
            div4.setAttribute("class", "row ml-5 my-2");
            form.appendChild(div4);

            //Create paragraph for block for unit 
            let block_for_unit = document.createElement("label");
            block_for_unit.setAttribute("for", id + "-block_for_unit");
            block_for_unit.setAttribute("class", "mr-2");
            block_for_unit.innerHTML = 'Number of blocks per unit: ';
            div4.appendChild(block_for_unit);
            //Create input for block for unit
            input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("style", "width: 5%;");
            input.setAttribute("id", id + "-block_for_unit");
            input.setAttribute("name", "block_for_unit");
            input.setAttribute("value", block_for_unit_value);
            div4.appendChild(input);


            //Create button to submit information
            let btn = document.createElement("input");
            //btn.setAttribute("onclick", 'updateCoursePlanInfo("' + coursePlanInfo[i].id_course + '" , "' + coursePlanInfo[i].school_year + '" , "' + coursePlanInfo[i].semester +'")');
            btn.setAttribute("role", "button");
            btn.setAttribute("type", "submit");
            btn.setAttribute("aria-pressed", "true");
            btn.setAttribute("class", "mx-5 btn btn-outline-warning mb-5 row");
            btn.setAttribute("value", "Update info");
            form.appendChild(btn);
        }
        
    
      }
    };
    xhttp.open("GET", "setCoursePlan", true);
    xhttp.send();
}

//update course plan hours info 
function updateCoursePlanInfo(id_course, school_year, semester){
    let initial_hour = document.getElementById(id_course + "-" + school_year + "-" + semester + "-initial_hour").value;
    let final_hour = document.getElementById(id_course + "-" + school_year + "-" + semester + "-final_hour").value;
    let block_duration = document.getElementById(id_course + "-" + school_year + "-" + semester + "-block_duration").value;
    let block_for_unit = document.getElementById(id_course + "-" + school_year + "-" + semester + "-block_for_unit").value;

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        alert('update sucessfull');
      }else{
        alert('update unsucessfull');
      }
    };
    xhttp.open("POST", "updateCoursePlanInfo", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("course=" + id_course + "&school_year=" + school_year + "&semester=" + semester + "&initial_hour=" + initial_hour + "&final_hour=" + final_hour + "&block_duration=" + block_duration + "&block_for_unit=" + block_for_unit);
}

//on change of select, change the type of class from day to night class or other way
function setClassType(){
    let value = document.getElementById("class_select").value;
    let context = document.getElementById("contextClasses").value;
    
    let splitted = context.split("-");
    let course = splitted[0];
    let school_year = splitted[1];
    let semester = splitted[2];
    let entity = splitted[3];


    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        let definitions = JSON.parse(this.responseText).definitions;

        //If there are definitions put the correct hours, if not say that there are none.
        if(definitions.length > 0){
            if(value === "night_class"){
                document.getElementById("definitions").innerHTML = "18:30-23:00 (01:30)";
            }else{
                document.getElementById("definitions").innerHTML = definitions[0] + "-" + definitions[1] + " (" + definitions[2] + ")";
            }
        }else{
            document.getElementById("definitions").innerHTML = "No hours settings yet";
        }


        document.getElementById("class_type").value = value;
        
    }
    };
    xhttp.open("POST", "setClassType", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&entity=" + entity + "&value=" + value);
}

//######################################################################




//#################### VIEW SHCEDULES ##############################
//
function loadSchedules(){

    let role = document.getElementById("role").value;

    if(role === "Teacher"){
        console.log(role);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            let result = JSON.parse(this.responseText).result;
            console.log(result);

            for(let i = 0; i < result.length; i++){


                
                let splittedDate = result[i].date_time.split("T");
                let hours = splittedDate[0];
            
                //Add 1 to days because of db delay
                let sum = parseInt(hours.split("-")[2]) + 1;
                if(sum === 31){
                    sum =1;
                }
            
                let rightHours =  hours.split("-")[0] + "-" + hours.split("-")[1] + "-" +  sum;
                let hoursForDB = rightHours + " 00:00:00";

            
                let whereToInsert = document.getElementById("whereToInsert");
                let div = document.createElement("div");
                div.setAttribute("class", "col d-flex justify-content-center mt-5");
                let a = document.createElement("a");
                a.setAttribute("href", "ShowScheduleTeacher/" + result[i].email + "&" +  hoursForDB);
                let div2 = document.createElement("div");
                div2.setAttribute("class", "card");
                div2.setAttribute("style", "width: 24rem;");
                let img = document.createElement("img");
                img.setAttribute("class", "card-img-top");
                img.setAttribute("src", result[i].schedule_url);
                let hr = document.createElement("hr");
                let div3 = document.createElement("div");
                div3.setAttribute("class", "card-body d-flex justify-content-center");
                let p = document.createElement("p");
                p.setAttribute("class", "card-text");

                p.innerHTML =  "Availability stain (" + rightHours + ")";

                whereToInsert.appendChild(div);
                div.appendChild(a);
                a.appendChild(div2);
                div2.appendChild(img);
                div2.appendChild(hr);
                div2.appendChild(div3);
                div3.appendChild(p);
                
            }

            }
        };
        xhttp.open("GET", "loadTeachersSchedules", true);
        xhttp.send();
    }else{
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            let result = JSON.parse(this.responseText).result;
            console.log(result);


            for(let i = 0; i < result.length; i++){
                result[i].schedule = result[i].schedule.toString();
                result[i].schedule_url = result[i].schedule_url.toString();

                console.log(result[i].schedule);

                if(result[i].schedule !== ""){
                    console.log(result[i].id + "-" + result[i].course_name + "-" + result[i].school_year + "-" + result[i].semester + "-" + result[i].class_name);
                    let whereToInsert = document.getElementById("whereToInsert");
                    let div = document.createElement("div");
                    div.setAttribute("class", "col d-flex justify-content-center mt-5");
                    let a = document.createElement("a");

                    let splittedSchoolYear = result[i].school_year.split("/");
                    let school_year = splittedSchoolYear[0] + "-" + splittedSchoolYear[1];

                    a.setAttribute("href", "ShowScheduleCoord/" + result[i].course_name + "-" + school_year + "-" + result[i].semester + "-" + result[i].class_name + "-" + result[i].id);
                    let div2 = document.createElement("div");
                    div2.setAttribute("class", "card");
                    div2.setAttribute("style", "width: 24rem;");
                    let img = document.createElement("img");
                    img.setAttribute("class", "card-img-top");
                    img.setAttribute("src", result[i].schedule_url);
                    let hr = document.createElement("hr");
                    let div3 = document.createElement("div");
                    div3.setAttribute("class", "card-body d-flex justify-content-center");
                    let p = document.createElement("p");
                    p.setAttribute("class", "card-text");
                    p.innerHTML = result[i].course_name + "-" + result[i].school_year + "-" + result[i].semester + "-" + result[i].class_name;

                    whereToInsert.appendChild(div);
                    div.appendChild(a);
                    a.appendChild(div2);
                    div2.appendChild(img);
                    div2.appendChild(hr);
                    div2.appendChild(div3);
                    div3.appendChild(p);
                }

                


            }

            }
        };
        xhttp.open("GET", "loadCoordSchedules", true);
        xhttp.send();
    }

    
}
//######################################################################




//#################### LOGIN AND REGISTERS ##############################
let login = document.getElementById("Login");
let signup = document.getElementById("SignUp");

//Toogle between login and sign up
function LoginToSignup(){
    login.hidden = !login.hidden;
    signup.hidden = !signup.hidden;
}

//Validate sign up
function validateSignUp(){
    //Username validation
    if(document.getElementById("usernameSignup").value === null || document.getElementById("usernameSignup").value === ""){
        alert("User name not valid!");
        return false;
    }
    //Password validation
    if(document.getElementById("passwordSignup").value === null || document.getElementById("passwordSignup").value === ""){
        alert("Password not valid!");
        return false;
    }
    //Confirm password validation
    if(document.getElementById("confirmPasswordSignup").value === null || document.getElementById("confirmPasswordSignup").value === ""){
        alert("Password confirmation not valid!");
        return false;
    }
    //Check if password and confirmation matches
    if(document.getElementById("passwordSignup").value != document.getElementById("confirmPasswordSignup").value){
        alert("Passwords doesn't match");
        return false;
    }
    //Email validation
    if(document.getElementById("emailSignup").value === null || document.getElementById("emailSignup").value === ""){
        alert("E-mail not valid!");
        return false;
    }
    //Roles validation
    let formValid = false;
    let i = 0;
    while (!formValid && i < document.getElementsByName("role").length) {
        if (document.getElementsByName("role")[i].checked) formValid = true;
        i++;        
    }
    if (!formValid) alert("Must check some role!");
    return formValid;    
}

//Validade login
function validateLogIn(){
    //Username validation
    if(document.getElementById("usernameLogin").value === null || document.getElementById("usernameLogin").value === ""){
        alert("User name not valid!");
        return false;
    }
    //Password validation
    if(document.getElementById("passwordLogin").value === null || document.getElementById("passwordLogin").value === ""){
        alert("Password not valid!");
        return false;
    }

    return true;
}
//#####################################################################




//#################### CLASS SCHEDULE ##############################
//Set side bar info, colors of each unit and counters of each unit to 0
function setSideBar(){
    let teachers = document.getElementById("justAdiv").innerHTML;
    let colors = document.getElementById("colors").value;
    let colorsArray = colors.split(",");

    let teach = JSON.parse(teachers);
    let units = []
    for (let i = 0; i < teach.length; i++) {
        units.push(teach[i][0]);
    }

    let unitsUnique = deleteDuplicates(units);

    for (let i = 0; i < unitsUnique.length; i++) {
        let ul = document.getElementById("weeklyHours");
        let li = document.createElement("li");
        li.setAttribute("style", "background-color:" +  colorsArray[i] +"; display=block; width: 100px;");
        li.setAttribute("class", "rounded p-1 mb-1");

        
        let inp = document.createElement("input");
        inp.setAttribute("type", "radio");
        inp.setAttribute("name", "subjectSelected");
        inp.setAttribute("onclick", "save_subjectCustom('"+ unitsUnique[i] +"', '"+ colorsArray[i] + "', '" + document.getElementById('justAdiv').innerHTML + "')");
        inp.setAttribute("class", "d-inline");
        inp.setAttribute("value", "HTML");
        inp.innerHTML = unitsUnique[i];

        li.innerHTML = inp.outerHTML + " " + unitsUnique[i];
        ul.appendChild(li);

        // Add counters
        let d = document.createElement("div");
        d.setAttribute("id", ""+ unitsUnique[i] +"");
        d.innerHTML = unitsUnique[i] + " : 0" + " / " + result[0].block_for_unit;

        let main = document.getElementById("limitHours");
        main.appendChild(d);
    }

    //Put each unit corresponding to the proper teacher
    for (let i = 0; i < teach.length; i++) {
        let ul = document.getElementById("associations");
        let li = document.createElement("li");
        li.innerHTML = teach[i][0] + " - " + teach[i][1];
        ul.appendChild(li);
    }


}
//#####################################################################




//################### AUX ####################################
function deleteDuplicates(array){
    let uniqueAux_id = [];
        array.forEach((c) => {
            if (!uniqueAux_id.includes(c)) {
                uniqueAux_id.push(c);
            }
        });

    return uniqueAux_id;
}
