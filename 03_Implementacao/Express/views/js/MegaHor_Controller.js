let schedule = null;
let result = null;
let bringLeft = true;
let previousStain = null;
let URL_email_string_UC_name = [];
let ORIGINALURL_email_string_UC_name = [];
let current_subject = "";
let current_color = "#ffc290";
let subjectColorDict = [];
let del = false;
let highlightedMatrix = null;

let classSubjectCount = [];
let maxNumberOfBlocks = null;

//#################### SAVE STAIN ################################
let saved = false;

// Transform table into image, download the image and display it on the page
function downloadtableToServer() {

    if(!saved){
        var node = document.getElementById('tableSchedule');
        domtoimage.toPng(node)
            .then(function (dataUrl) {
                let h5 = document.createElement("h5");
                h5.setAttribute("class", "my-3");
                h5.innerHTML = "Unavailability stain";
                document.getElementById("unavailabilityContainer").appendChild(h5);

                let img = document.createElement("IMG");
                img.setAttribute("src", dataUrl);
                img.setAttribute("width", "auto");
                img.setAttribute("height", "auto");
                img.setAttribute("alt", "schedule");
                document.getElementById("unavailabilityContainer").appendChild(img);
                
                schedule.matrix = unavailabilityToAvailability(schedule.matrix);
                schedule.matrix_to_html(schedule.matrix);
                domtoimage.toPng(node)
                    .then(function (dataUrl) {
                        schedule.set_image(dataUrl); // Save image in schedule instance

                        h5 = document.createElement("h5");
                        h5.setAttribute("class", "mt-5");
                        h5.innerHTML = "Availability stain";
                        document.getElementById("unavailabilityContainer").appendChild(h5);

                        img = document.createElement("IMG");
                        img.setAttribute("src", dataUrl);
                        img.setAttribute("width", "auto/2");
                        img.setAttribute("height", "auto/2");
                        img.setAttribute("alt", "schedule");
                        img.setAttribute("class", "mb-5");
                        document.getElementById("unavailabilityContainer").appendChild(img);
                        downloadURI(dataUrl, "availabilityStain.png");
                        saveStain();
                        schedule.matrix = unavailabilityToAvailability(schedule.matrix);
                        schedule.matrix_to_html(schedule.matrix);

                        document.getElementById("saveStain").hidden = true;
                        saved = true;
                    })
                    .catch(function (error) {
                        console.error('oops, something went wrong!', error);
                    });

            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
        }
}

// Reload page in order to restart the stain creation process
function restartStain(){
    window.location.reload();
}

// Download image
function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}

// Download image as Jpeg and save it in schedule instance
function downloadtable() {
    var node = document.getElementById('tableSchedule');
    domtoimage.toJpeg(node, { quality: 0.95 })
        .then(function (dataUrl) {
            schedule.set_image(dataUrl); //Save image in schedule instance
            downloadURI(dataUrl, "records.jpeg");
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        }); 
  }

// Transform unavailability stain into availability stain
function unavailabilityToAvailability(matrix){
    //Check the name and color thats in unavailability
    let nameToChange;
    loop1:
    for(let i = 0; i <matrix.length; i++){
        for(let j = 0; j < 8; j++){
            if(j >= 1){
                if(matrix[i][j] !== "null"){
                    nameToChange = matrix[i][j];
                    console.log(nameToChange);
                    break loop1;
                }
            }
        }
    }

    let inversed = [];
    for(let i = 0; i <matrix.length; i++){
        let row = [];
        for(let j = 0; j < 8; j++){
            if(j < 1){
                row[j] = matrix[i][j];
            }else{
                if(matrix[i][j] === "null"){
                    row[j] = nameToChange;
                }else{
                    row[j] = "null";
                }
            }
        }
        inversed.push(row);
    }

    return inversed;
}

// Save stain in database
function saveStain(){
    let teacherName = document.getElementById("teacherName").innerHTML;
    let teacherEmail = document.getElementById("teacherEmail").innerHTML; 

    if(teacherEmail === 'teacher'){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {     
        if (this.readyState == 4 && this.status == 200) {
            let stain = JSON.parse(this.responseText).stain;
            let matrix = JSON.parse(this.responseText).matrix;
            let Name = JSON.parse(this.responseText).teacherName;
        }
        };
        xhttp.open("POST", "saveStain", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("matrix=" + schedule.matrix + "&stain=" + encodeURIComponent(schedule.scheduleAsImage) + "&teacherName=" + teacherName + "&teacherEmail=" + teacherEmail);
    }else{
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {     
        if (this.readyState == 4 && this.status == 200) {
            let stain = JSON.parse(this.responseText).stain;
            let matrix = JSON.parse(this.responseText).matrix;
            let Name = JSON.parse(this.responseText).teacherName;
        }
        };
        xhttp.open("POST", "../saveStainCoord", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("matrix=" + schedule.matrix + "&stain=" + encodeURIComponent(schedule.scheduleAsImage) + "&teacherName=" + teacherName + "&teacherEmail=" + teacherEmail);
    }
    
    

}

//####################################################

//Create a new schedule
function new_schedule(param){
    schedule = new Schedule("9:30", "23:00", "1:30", param);
    schedule.matrix_to_html(schedule.matrix);
}


// Define initial hours, final hours and interval
function set_hours(param){
    let c = confirm("Are you sure you want loose all your work?");
    if(c){
        let starting_hour = document.getElementById("starting_hour").value;
        let ending_hour = document.getElementById("ending_hour").value;
        let interval_hour = document.getElementById("interval").value;

        schedule.reset_table();

        console.log(starting_hour);
        console.log(ending_hour);
        console.log(interval_hour);

        schedule = new Schedule(starting_hour, ending_hour, interval_hour, param);
        schedule.matrix_to_html(schedule.matrix);
    }

}

// Define current subject and current color to be used when clicking on a cell
function save_subject() {
    current_subject = document.getElementById("current_subject").value;
    current_color = document.getElementById("colorPicker").value;
    subjectColorDict.push({
        key:   current_subject,
        value: current_color
    });

    //Change in database
    schedule.save_subject();
}

// Define current subject and automatically place correspondant stains in buttons
function save_subjectCustom(cur_sub, color, t) {
    document.getElementById("toCreateInfo").innerHTML = "You have choosen " + cur_sub;
    current_subject = cur_sub;
    current_color = color;
    subjectColorDict.push({
        key:   current_subject,
        value: current_color
    });

    let teach = JSON.parse(t);
    
    // Obtain subject stains
    let subjectStains = [];
    let teachers = [];
    for (let i = 0; i < teach.length; i++) {
        if(teach[i][0] === current_subject){
            subjectStains.push(teach[i][3]); // Add URL
            teachers.push(teach[i][1]); // Add teacher name
        }

        // Save elements in ORIGINAL array
        let ORIGINALelements = [teach[i][3], teach[i][4], teach[i][2], teach[i][0], teach[i][1]];
        ORIGINALURL_email_string_UC_name.push(ORIGINALelements);

        // Cut before saving as string
        teach[i][2] = cutMatrix(teach[i][2]);

        // Save elements in array
        let elements = [teach[i][3], teach[i][4], teach[i][2], teach[i][0], teach[i][1]];
        URL_email_string_UC_name.push(elements);
    }

    // Clear buttons before placing stains
    for (let y = 0; y < 4; y++) {
        console.log("plus"+ (y+1) +"");
        document.getElementById("x"+ (y+1) +"").hidden = true;
        document.getElementById("img"+ (y+1) +"").hidden = true;
        document.getElementById("plus"+ (y+1) +"").hidden = false;
        document.getElementById("p"+ (y+1) +"").hidden = true;
    }

    // Traverse buttons and place stain images
    for (let x = 0; x < subjectStains.length; x++) {
        console.log("plus"+ (x+1) +"");
        let btn = document.getElementById("plus"+ (x+1) +"");
        place_image(btn.parentElement, subjectStains[x], teachers[x], "nao ha");
    }


}

// Place image in 'plus' button
function place_image(param, url, teacher, subject){
    param.children[0].hidden = false;
    param.children[0].innerHTML = teacher;
    param.children[1].hidden = true;
    param.children[2].hidden = false;
    param.children[2].setAttribute("src", url);
    param.children[3].hidden = false;
}

// Fill modalUl
function modalUL(param, teachers, whereToPlace) {

    let destination = whereToPlace.parentElement;
    document.getElementById("modalTitle").innerHTML="Select the schedule you want to compare:";
    // Remove already existing elements
    let ul = document.getElementById("uList");
    while( ul.firstChild ){
        ul.removeChild( ul.firstChild );
      }

    let teach = JSON.parse(teachers);

    let current_subject = teach[0][0];

    // Create subject caption
    let new_cap = document.createElement("div");
    new_cap.innerHTML = current_subject;
    new_cap.setAttribute('style', "font-size: 200%;");
    param.appendChild(new_cap);


    // let divs = ['firstdiv', 'seconddiv', 'thirddiv', 'forthdiv', 'fifthdiv', 'sixthdiv', 'seventhdiv', 'eightdiv'];
    for (let i = 0; i < teach.length; i++) {
        // Create img
        let new_img = document.createElement("img");
        new_img.src = teach[i][3];
        new_img.width = "250";
        new_img.height = "150";
        new_img.alt = "Schedule";
        new_img.setAttribute('onclick', "place_image(document.getElementById('" + destination.id + "'), '" + teach[i][3] + "', '"+ teach[i][1] +"', '"+ teach[i][0] +"'); modal.style.display = 'none';");        
        new_img.setAttribute('style', "margin-bottom: 25px;");


        // Save elements in ORIGINAL array
        let ORIGINALelements = [teach[i][3], teach[i][4], teach[i][2], teach[i][0], teach[i][1]];
        ORIGINALURL_email_string_UC_name.push(ORIGINALelements);

        // Cut before saving as string
        teach[i][2] = cutMatrix(teach[i][2]);

        // Save elements in array
        let elements = [teach[i][3], teach[i][4], teach[i][2], teach[i][0], teach[i][1]];
        URL_email_string_UC_name.push(elements);

        // Create image caption
        let new_caption = document.createElement("div");
        new_caption.innerHTML = teach[i][1];

        // If subject changed, update it and create caption
        if(teach[i][0] !== current_subject){
            // HR
            let new_hr = document.createElement("hr");
            new_hr.setAttribute('style', "height:5px; background-color:black; margin-bottom: 50px;");
            param.appendChild(new_hr);

            current_subject = teach[i][0]; // update subject
            let new_sub = document.createElement("div");
            new_sub.innerHTML = current_subject;
            new_sub.setAttribute('style', "font-size: 200%;");
            param.appendChild(new_sub);
        }

        // Create li
        let new_li = document.createElement("li");
        new_li.appendChild(new_caption);
        new_li.appendChild(new_img);

        param.appendChild(new_li);
    }    
    
}

// Convert from URL to matrix's string
function URL_to_string(param){

    // Find string that corresponds to the dragged image's URL
    for (let i = 0; i < URL_email_string_UC_name.length; i++) {
        if (param === URL_email_string_UC_name[i][0]) {
            return URL_email_string_UC_name[i][2];
        }
    }

    return null;
}



// Given the image URL, return the teacher's parameters
function returnParametersTeacher(param){

    // Find string that corresponds to the dragged image's URL
    for (let i = 0; i < URL_email_string_UC_name.length; i++) {
        if (param === URL_email_string_UC_name[i][0]) {
            return URL_email_string_UC_name[i];
        }
    }

    return null;
}



// Edit the pressed cell
function edit_cell(cell) {

    let isClassSchedule = document.getElementById("isClassSchedule");
    let previousString = cell.innerHTML.split("-")[1];
    if(previousString !== undefined){
        previousString = previousString.slice(1); // Eliminate first space character from string
    }
    if(previousString === undefined){
        previousString = cell.innerHTML;
    }
    
    
    //Condition to force user to pick a subject
    if (current_subject != "") {
        // Reset all cell's borders and color
        for (let index = 0; index < document.getElementById("tbody").children.length; index++) {
            document.getElementById("tbody").children[index].children[cell.cellIndex].style.removeProperty('border-bottom');
            document.getElementById("tbody").children[index].children[cell.cellIndex].style.removeProperty('border-top');
            document.getElementById("tbody").children[index].children[cell.cellIndex].style.removeProperty('color');
        }


        if(!del && cell.parentElement.parentElement.id !== "tbody2"){
            cell.innerHTML = current_subject;
        }
        

        // If delete is enabled, text and style is ""
        if(del && (cell.parentElement.parentElement.id === "tbody2" || isClassSchedule.value === "no")){
            console.log("ENTROU");
            cell.innerHTML = "";
            cell.setAttribute("style", "");
        }

        // Only set background color if delete is disabled
        if(!del && cell.parentElement.parentElement.id !== "tbody2"){
        cell.setAttribute("style", "background-color: "+ current_color + ";");
        }

        // Print coordenates of current (clicked) cell
        console.log("row -> " + cell.parentElement.rowIndex);
        console.log("column -> " + cell.cellIndex);



    if(del && cell.parentElement.parentElement.id === "tbody2"){
        // Remove last element of class array
        for (let i = 0; i < classSubjectCount.length; i++) {

            console.log("TO DELETE: " + previousString);
            console.log("===: ");
            console.log("IN ARRAY: " + classSubjectCount[i][0]);
            // Find subject and add cell if there's room
            if(classSubjectCount[i][0] === previousString){
                classSubjectCount[i].pop();
            }
        }

        // Increment counter visually
        let main = document.getElementById("limitHours");
        for (let u = 0; u < main.children.length; u++) {
            let c = main.children[u];
            let oldCounter = (c.innerHTML).split(" ");
            if(previousString === oldCounter[0]){
                let number = parseInt(oldCounter[2]) - 1; // Decrement subject block
                let finalString = oldCounter[0] + " " + oldCounter[1] + " " + number + " " + oldCounter[3] + " " + oldCounter[4];
                c.innerText = finalString; // Update
            }

        }

    }



    

    if(isClassSchedule.value === "yes" && !del){
        let copyPreviousStain = schedule_tbody.matrix_to_string(); // Copy matrix before changes
        schedule_tbody.update_matrix(); // Update matrix with subject placed
        // Merge
        intersectionWithoutMerge(schedule_tbody.matrix, string_to_matrix(copyPreviousStain));

        // Only update if it can be placed and if there's room to insert
        // Find teacher name for matrix
        let foundTeacherName = "no name";
        for (let z = 0; z < URL_email_string_UC_name.length; z++) {
            if(highlightedMatrix.toString() === URL_email_string_UC_name[z][2]){ // Compare matrices
                foundTeacherName = URL_email_string_UC_name[z][4];
                break;
            }
        }

        let doesTeach = false;
        // Check if found teacher teaches the current subject
        for (let z = 0; z < URL_email_string_UC_name.length; z++) {
            if(highlightedMatrix.toString() === URL_email_string_UC_name[z][2] && current_subject === URL_email_string_UC_name[z][3]){ // Compare matrices
                doesTeach = true;
                break;
            }
        }


        if(doesTeach && intersect_with_HIGHLIGHTED_stains(cell.parentElement.rowIndex-1,cell.cellIndex) && cell.parentElement.parentElement.id !== "tbody2"){
            if(findSubjectAndIncrement(current_subject)){
                
                let strToPlaceWithChosenColor = foundTeacherName + " - " + current_subject + "%" + current_color;
                schedule_tbody2.matrix[cell.parentElement.rowIndex-1][cell.cellIndex] = strToPlaceWithChosenColor;

                // Update schedule on the right
                schedule_tbody2.matrix_to_html(schedule_tbody2.matrix);                                
            }

            schedule_tbody.matrix = string_to_matrix(copyPreviousStain);

        }
        else{
            // Replace with old matrix
            schedule_tbody.matrix = string_to_matrix(copyPreviousStain);
        }
        
        schedule_tbody.matrix_to_html(schedule_tbody.matrix); // Update HTML

    }
    else{
        schedule.update_matrix();
        schedule.matrix_to_html(schedule.matrix);
    }
    
    }else{
        alert("You need to choose a subject to place.");
    }
    console.log(URL_email_string_UC_name);
}

// Check if coordinates intersect with highlighted stain
function intersect_with_HIGHLIGHTED_stains(x, y){
    // Matrix with the newly placed subject
    // If lower schedule as something, then ignore
    if(schedule_tbody2.matrix[x][y] !== "null"){
        return false;
    }

    if(isIntersected(x, y, highlightedMatrix)){
        
        return true;
    }

    return false;
}

// Delete cell and change delete button's color
function delete_cell(){
    del = !del;
    console.log(del);
    if(del){
        document.getElementById("delete_button").setAttribute("class", "col col-sm-1 btn btn-danger px-5 bd-highlight d-flex justify-content-center mx-2");
        document.getElementById("set_button").setAttribute("style", "visibility: hidden;");
    }
    else{
        document.getElementById("delete_button").setAttribute("class", "col col-sm-1 btn btn-outline-danger px-5 bd-highlight d-flex justify-content-center mx-2");
        document.getElementById("set_button").setAttribute("style", "");
    }
}

// Delete cell and update matrix
function delete_cell_class_schedule(){
    del = !del;
    console.log(del);
    if(del){
        document.getElementById("delete_button").setAttribute("class", "btn btn-danger ml-2 d-inline");
    }
    else{
        document.getElementById("delete_button").setAttribute("class", "btn btn-outline-danger ml-2 d-inline");
    }

    if(current_subject === ""){
        current_subject = "empty";
    }

    schedule_tbody2.update_matrix();
    schedule_tbody2.matrix_to_html(schedule_tbody2.matrix);

}


  // Split string by commas
  // Split 7 by 7 (days of week)
  // Return matrix representation
  function string_to_matrix(strTable){
    let str_splitted = strTable.split(",");
    let new_mtx = [];

    // Delete line by line and return deleted line to then be inserted in matrix
    while(str_splitted.length) new_mtx.push(str_splitted.splice(0,8));

    return new_mtx;
  }

// Intersect 2 matrices (matrix2 is placed on top of matrix1)
// But only if it's different than null
function intersection(matrix1, matrix2){

for (let row = 0; row < matrix1.length; row++) {
    for (let col = 1; col < 8; col++) {
        if(matrix1[row][col] !== "null" || matrix2[row][col] !== "null"){ // If any space has anything
            if(matrix1[row][col] !== "null" && matrix2[row][col] !== "null"){ // If both spaces have something

            }
            else if(matrix2[row][col] !== "null"){ // If only the old matrix has something
                matrix1[row][col] = matrix2[row][col]; // Update matrix to place
            }
        }
    }
  }
}

// Intersect 2 matrices (matrix2 is placed on top of matrix1)
function intersectionToLeft(matrix1, matrix2){

  for (let row = 0; row < matrix1.length; row++) {
      for (let col = 1; col < 8; col++) {
          if(matrix1[row][col] !== "null" || matrix2[row][col] !== "null"){ // If any space has anything
              if(matrix1[row][col] !== "null" && matrix2[row][col] !== "null"){ // If both spaces have something
                  matrix1[row][col] = matrix2[row][col];
              }
              else if(matrix2[row][col] !== "null"){ // If only the old matrix has something
                  matrix1[row][col] = matrix2[row][col]; // Update matrix to place
              }
          }
      }
  }
}


// Intersect 2 matrices (matrix2 is placed on top of matrix1)
// But each cell shows the strings of both matrices
function intersectionWithoutMerge(matrix1, matrix2){

  for (let row = 0; row < matrix1.length; row++) {
      for (let col = 1; col < 8; col++) {
          if(matrix1[row][col] !== "null" || matrix2[row][col] !== "null"){ // If any space has anything
              if(matrix1[row][col] !== "null" && matrix2[row][col] !== "null"){ // If both spaces have something and are different
                  console.log(matrix2[row][col] + "%" + matrix1[row][col]);
                  let splitted = (matrix2[row][col] + "%" + matrix1[row][col]).split("%");
                  let str = splitted[0] + "-" +splitted[2] + "%" + splitted[1];
                  if(splitted[0] !== splitted[2]){
                    matrix1[row][col] = str; // Update matrix to place with a warning
                  }
              }
              else if(matrix2[row][col] !== "null"){ // If only the old matrix has something
                  matrix1[row][col] = matrix2[row][col]; // Update matrix to place
              }
          }
      }
  }
}


// Return true if coordinates intersect anything different than 'null' in the matrix
function isIntersected(x, y, matrix2){

  if(matrix2[x][y] !== "null"){ // If both spaces have something
    return true;
  }

  return false;
}


// Fill schedule with empty matrix
function clearSchedule(scheduleToDelete){
    let dummySchedule = new Schedule(result[0].initial_hour, result[0].final_hour, result[0].block_duration, document.getElementById("tbody"));
    let string = dummySchedule.matrix_to_string();
    console.log(string_to_matrix(string));
    console.log("clear");

    let dummySchedule2 = new Schedule(result[0].initial_hour, result[0].final_hour, result[0].block_duration, document.getElementById("tbody"));
    highlightedMatrix = dummySchedule2.matrix;

    scheduleToDelete.matrix_to_html(string_to_matrix(string)); // Show matrix with conflicts in HTML
    scheduleToDelete.matrix = string_to_matrix(string); // Save normal matrix in instance

    //Delete from HTML
    let line = document.getElementById("active");
    while( line.firstChild ){
        line.removeChild( line.firstChild );
    }
}



// Save class schedule in database
function saveClassSchedule(){
    var node = document.getElementById('tableSchedule');
    var img;
    domtoimage.toJpeg(node, { quality: 0.95 })
        .then(function (dataUrl) {
            schedule_tbody2.set_image(dataUrl); //Save image in schedule instance
            downloadURI(dataUrl, "records.jpeg");

            let context = document.getElementById("classID").value;
            let splitted = context.split("-");
            let course = splitted[0];
            let school_year = splitted[1];
            let semester = splitted[2];
            let entity = splitted[3];

            let type = document.getElementById("class_type").value;


            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) { 
                    //Remove from HTML
                    console.log("saved");
                }
            };
            xhttp.open("POST", "saveClassSchedule", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&entity=" + entity + "&schedule=" + schedule_tbody2.matrix.toString() + "&scheduleURL=" + encodeURIComponent(schedule_tbody2.scheduleAsImage) + "&class_type=" + type);


        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        }); 

    
}

// Save subjects to be used in the class creation in a global array
function defineCoursePlan(plan, teachers){

    maxNumberOfBlocks = parseInt(plan) + 1;

    // classSubjectCount
    let teach = JSON.parse(teachers);

    // Create an array for each subject
    for (let i = 0; i < teach.length; i++) {

        let notAdded = true;
        // Check if subject was already added
        for (let x = 0; x < classSubjectCount.length; x++) {
            if(classSubjectCount[x][0] === teach[i][0]){
                notAdded = false;
            }
        }

        if(notAdded){
            // Place subject and teacher
            let new_arr = [teach[i][0]];
            classSubjectCount.push(new_arr);
        }

    }

}


// Try to increment the subject 
// Return true if incremented
function findSubjectAndIncrement(subject){
    for (let i = 0; i < classSubjectCount.length; i++) {
        // Find subject and add cell if there's room
        if(classSubjectCount[i][0] === subject && classSubjectCount[i].length < maxNumberOfBlocks){
            
            classSubjectCount[i].push(subject);

            // Increment counter visually
            let main = document.getElementById("limitHours");
            for (let u = 0; u < main.children.length; u++) {
                let c = main.children[u];
                let oldCounter = (c.innerHTML).split(" ");
                if(subject === oldCounter[0]){
                    let number = parseInt(oldCounter[2]) + 1; // Decrement subject block
                    let finalString = oldCounter[0] + " " + oldCounter[1] + " " + number + " " + oldCounter[3] + " " + oldCounter[4];
                    c.innerText = finalString; // Update li
                }

            }

            return true; // Inserted
        }
    }


    alert("No more time blocks available for this subject! You will need to remove an existing block.");
    return false; // No more room to insert
}



///CLASS SCHEDULE JS

// Open nav bar
function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
// Close nav bar
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  }
  

let grabbed_schedule_URL = null;
let schedule_tbody = null;
let schedule_tbody2 = null;

// Class schedule page initialization
function loadSchedules() {
    
    let classID = document.getElementById("classID").value;
    
    let splitted = classID.split("-");
    let course = splitted[0];
    let school_year = splitted[1];
    let semester = splitted[2];
    let entity = splitted[3];

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        result = JSON.parse(this.responseText).result;
        console.log(result);


        if(document.getElementById("class_type").value === "night_class"){
            result[0].initial_hour = "18:30";
            result[0].final_hour = "23:00";
        }


        let dummySchedule = new Schedule(result[0].initial_hour, result[0].final_hour, result[0].block_duration, document.getElementById("tbody"));
        highlightedMatrix = dummySchedule.matrix.slice();

        schedule_tbody = dummySchedule;
        schedule_tbody2 = new Schedule(result[0].initial_hour, result[0].final_hour, result[0].block_duration, document.getElementById("tbody2"));
        blank_stain_string = schedule_tbody.matrix_to_string();
        defineCoursePlan(result[0].block_for_unit, document.getElementById("justAdiv").innerHTML);
        setSideBar();

        let existingSchedule = document.getElementById("class_schedule");
        if(existingSchedule.value !== ""){
            let matrixToPlace = string_to_matrix(elongateMatrix(existingSchedule.value));
            let matrixToPlaceCopy = matrixToPlace.slice(); // Copy
        
            intersection(matrixToPlaceCopy, schedule_tbody2.matrix); // Merge the two matrices
            schedule_tbody2.matrix_to_html(matrixToPlaceCopy); // Show matrix with conflicts in HTML
            schedule_tbody2.matrix = matrixToPlace; // Save normal matrix in instance

            clearSchedule(schedule_tbody);
            updateCountersFirstTime();
        }

    }
    };
    xhttp.open("GET", "getClassDefinitions?course=" + course + "&school_year=" + school_year + "&semester=" + semester + "&entity=" + entity, true);
    xhttp.send();

}


function allowDrop(ev) {
    ev.preventDefault();
}


// ####### DRAG AND DROP #########

// Drag image to table
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    // Declare global instance as the clicked schedule URL
    grabbed_schedule = document.getElementById(ev.target.id).src;
    
}


// Drop image on table, transforming it into matrix format
function drop(ev, el, str) {
    ev.preventDefault();
    try {
    // Place global schedule instance
    console.log(returnParametersTeacher(grabbed_schedule)[3]);
    console.log(returnParametersTeacher(grabbed_schedule)[4]);
    let ucName = returnParametersTeacher(grabbed_schedule)[3];
    let teacherName = returnParametersTeacher(grabbed_schedule)[4];

    // Append teacher name if it doesn't exist already
    let line = document.getElementById("active");
    let exists = false;
    for (let i = 0; i < line.children.length; i++) {
        if(line.children[i].innerHTML === teacherName){
            exists = true;
            break;
        } 
    }

    if(!exists){

        for( i=0; i< line.children.length; i++ ){
            let button = line.children[i];
            button.setAttribute("style",""); 
        }

        var btn = document.createElement("BUTTON");
        btn.setAttribute("onclick", "placeStain('" + teacherName + "')");
        btn.setAttribute("style","background-color:lightgrey;"); 
        btn.innerHTML = teacherName;
        line.appendChild(btn);
    }


    // el
    if(str === 'left'){
        //string_to_matrix(URL_to_string(grabbed_schedule)) //MATRIX 1
        //schedule_tbody.matrix //MATRIX 2
        // MATRIX 1 + MATRIX 2
        // place result
        let matrixToPlace = string_to_matrix(URL_to_string(grabbed_schedule));

        highlightedMatrix = string_to_matrix(URL_to_string(grabbed_schedule)); // Save highlighted matrix
        // Dim other stain colors
        dimStains();
        let matrixToPlaceCopy = matrixToPlace.slice(); // Place the most recent stain on top of the result

        intersection(matrixToPlaceCopy, schedule_tbody.matrix); // Merge the two matrix
        schedule_tbody.matrix_to_html(matrixToPlaceCopy); // Show matrix with conflicts in HTML
        schedule_tbody.matrix = matrixToPlace; // Save normal matrix in instance
        console.log(matrixToPlace);
        console.log("LEFTTTT: ");
    }
    
    }
    catch(err) {
    }
}

// Dim the colors of all cells in left table
function dimStains(){
    // Traverse HTML
    for (let row = 0; row < schedule_tbody.matrix.length; row++) {
        for (let col = 1; col < 8; col++) {
            if(schedule_tbody.matrix[row][col] !== "null"){
                let arr = schedule_tbody.matrix[row][col].split("%");
                let oldColor = arr[1];
                let newColor = null;
                console.log("OLD COLOR: " + oldColor);
                if(oldColor.length === 7){
                    newColor = oldColor + "44";
                }
                else{
                    newColor = oldColor.slice(0,7) + "44";
                }
                console.log("NEW COLOR: " + newColor);
                let str = arr[0] + "%" + newColor;
                console.log("STR: " + str);
                schedule_tbody.matrix[row][col] = str;
            }
        }
    }

    // Update html
    schedule_tbody.matrix_to_html(schedule_tbody.matrix);
}


// Place the wanted teacher's stain, but dim everything before doing so
// Also change the separators color, to the respective teacher's name
function placeStain(teacher){
    let grabbed_schedule = null;
    // Find string that corresponds to the teacher's name
    for (let i = 0; i < URL_email_string_UC_name.length; i++) {
        if (teacher === URL_email_string_UC_name[i][4]) {
            grabbed_schedule = URL_email_string_UC_name[i][2];
            break;
        }
    }

    console.log(grabbed_schedule);

    highlightedMatrix = string_to_matrix(grabbed_schedule); // Save highlighted matrix

    let matrixToPlace = string_to_matrix(grabbed_schedule);
    // Dim other stain colors
    dimStains();
    let matrixToPlaceCopy = matrixToPlace.slice(); // Copy

    intersection(matrixToPlaceCopy, schedule_tbody.matrix); // Merge the two matrices
    schedule_tbody.matrix_to_html(matrixToPlaceCopy); // Show matrix with conflicts in HTML
    schedule_tbody.matrix = matrixToPlace; // Save normal matrix in instance
    console.log("LEFTTTT: ");

    //Delete from HTML
    let line = document.getElementById("active").children;
    console.log(line);


    for( i=0; i< line.length; i++ )
    {
        let button = line[i];
        if(button.innerHTML === teacher){
            button.setAttribute("style","background-color:lightgrey;");
        }else{
            button.setAttribute("style","");
        }
     
    }

}

// UnavailabilityStain page initialization
function onloadUnavailabilityStain(){

    let teacher = document.getElementById('teacherName').innerHTML;
    let scheduleToPut = document.getElementById('schedule').innerHTML;

    console.log(teacher);
    console.log(scheduleToPut);

    //make a new schedule
    new_schedule(document.getElementById('tbody')); 

    let hexColor;
    if(scheduleToPut === ""){
        //Pick a random color
        hexColor = Math.floor(Math.random()*16777215).toString(16);

        //Pass to rgb and check if it is too dark
        let c = hexColor.substring(1);      // strip #
        let rgb = parseInt(c, 16);   // convert rrggbb to decimal
        let r = (rgb >> 16) & 0xff;  // extract red
        let g = (rgb >>  8) & 0xff;  // extract green
        let b = (rgb >>  0) & 0xff;  // extract blue
        let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

        //If it is too dark, random color again
        while (luma < 40) {
            hexColor = Math.floor(Math.random()*16777215).toString(16);
            c = hexColor.substring(1);      // strip #
            rgb = parseInt(c, 16);   // convert rrggbb to decimal
            r = (rgb >> 16) & 0xff;  // extract red
            g = (rgb >>  8) & 0xff;  // extract green
            b = (rgb >>  0) & 0xff;  // extract blue

            luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
        }
    }else{

        //Pass availabilityStain to unavailability stain
        let availabilityStain = string_to_matrix(scheduleToPut);
        let unavailabilityStain = unavailabilityToAvailability(availabilityStain);

        //Put that matrix in the html
        schedule.matrix = unavailabilityStain;
        schedule.matrix_to_html(schedule.matrix);

        let nameToChange;
        for(let i = 0; i <unavailabilityStain.length; i++){
            for(let j = 0; j < 8; j++){
                if(j >= 1){
                    if(unavailabilityStain[i][j] !== "null"){
                        nameToChange = unavailabilityStain[i][j];
                        console.log(nameToChange);
                        break;
                    }
                }
            }
        }

        hexColor = nameToChange.split("#")[1];

    }

        current_subject = teacher + '%#'+ hexColor;

        console.log(current_subject);
    
}


// Show table on the right on top of left table (aka Show chosen subjects)
function sendToLeft(){
    if(bringLeft){
        document.getElementById("showChoosenSubject").setAttribute("class", "btn btn-info ml-2 d-inline");
        previousStain = schedule_tbody.matrix_to_string(); // Copy matrix before changes
        intersectionToLeft(schedule_tbody.matrix, schedule_tbody2.matrix);
        schedule_tbody.matrix_to_html(schedule_tbody.matrix); // Update HTML
        bringLeft = false;
    }
    else{
        document.getElementById("showChoosenSubject").setAttribute("class", "btn btn-outline-info ml-2 d-inline");
        schedule_tbody.matrix = string_to_matrix(previousStain);
        schedule_tbody.matrix_to_html(schedule_tbody.matrix); // Update HTML
        bringLeft = true;
    }

}


// Given a small matrix, return a matrix with classSchedule's page max size
function elongateMatrix(matrixToCut){

    let dummySchedule = new Schedule(result[0].initial_hour, result[0].final_hour, result[0].block_duration, document.getElementById("tbody"));
    let emptyMatrix = dummySchedule.matrix;

    // Convert string to matrix
    matrixToCut = string_to_matrix(matrixToCut);

    let mtxStart = matrixToCut[0][0].split(" ")[0];
    let mtxEnd = matrixToCut[matrixToCut.length - 1][0].split(" ")[2];

    // Traverse matrix and save only between start and end
    let startSaving = false;
    let mtxIndex = 0;
    for (let i = 0; i < emptyMatrix.length; i++) {

        // Start recording when matrix initial time coincides with current initial time
        console.log(emptyMatrix[i][0].split(" ")[0]);
        console.log(mtxStart);

        if(emptyMatrix[i][0].split(" ")[0] === mtxStart){
            // Find index
            for (let x = 0; x < matrixToCut.length; x++) {
                if(matrixToCut[x][0].split(" ")[0] === emptyMatrix[i][0].split(" ")[0]){
                    mtxIndex = x; // Save index with the supposed start time
                    break;
                }
            }
            startSaving = true;
        }

        // If it can be saved
        if(startSaving){
            emptyMatrix[i] = matrixToCut[mtxIndex];
            mtxIndex++;
        }

        // Stop recording when matrix initial time coincides with current ending time
        if(emptyMatrix[i][0].split(" ")[2] === mtxEnd){
            startSaving = false;
            break;
        }
        
    }

    return emptyMatrix.toString();

}

// Given a small matrix, return a matrix with the max size
function elongateMatrixTeacherStain(matrixToCut){

    let dummySchedule = new Schedule("9:30", "23:00", "1:30", document.getElementById("tbody"));
    let emptyMatrix = dummySchedule.matrix;

    let mtxStart = matrixToCut[0][0].split(" ")[0];
    let mtxEnd = matrixToCut[matrixToCut.length - 1][0].split(" ")[2];

    // Traverse matrix and save only between start and end
    let startSaving = false;
    let mtxIndex = 0;
    for (let i = 0; i < emptyMatrix.length; i++) {

        // Start recording when matrix initial time coincides with current initial time
        if(emptyMatrix[i][0].split(" ")[0] === mtxStart){
            // Find index
            for (let x = 0; x < matrixToCut.length; x++) {
                if(matrixToCut[x][0].split(" ")[0] === emptyMatrix[i][0].split(" ")[0]){
                    mtxIndex = x; // Save index with the supposed start time
                    break;
                }
            }
            startSaving = true;
        }

        // If it can be saved
        if(startSaving){
            emptyMatrix[i] = matrixToCut[mtxIndex];
            mtxIndex++;
        }

        // Stop recording when matrix initial time coincides with current ending time
        if(emptyMatrix[i][0].split(" ")[2] === mtxEnd){
            startSaving = false;
            break;
        }
        
    }

    return emptyMatrix.toString();

}


// Given a bigger matrix than classSchedule's page max size, return a properly cut matrix
function cutMatrix(matrixToCut){

    // Convert string to matrix
    matrixToCut = string_to_matrix(matrixToCut);

    let mtxStart = matrixToCut[0][0].split(" ")[0];
    let mtxEnd = matrixToCut[matrixToCut.length - 1][0].split(" ")[2];

    // Compare both starting times and check if html starts later
    let htmlHour = parseInt(schedule_tbody.matrix[0][0].split(":")[0]);
    let mtxHour = parseInt(mtxStart.split(":")[0]);

    // Traverse matrix and save only between start and end
    let finalMtx = [];
    let startSaving = false;
    let mtxIndex = 0;
    for (let i = 0; i < schedule_tbody.matrix.length; i++) {

        // Start recording when matrix initial time coincides with current initial time
        if(schedule_tbody.matrix[i][0].split(" ")[0] === mtxStart || (htmlHour > mtxHour)){
            // Find index
            for (let x = 0; x < matrixToCut.length; x++) {
                if(matrixToCut[x][0].split(" ")[0] === schedule_tbody.matrix[i][0].split(" ")[0]){
                    mtxIndex = x; // Save index with the supposed start time
                    break;
                }
            }
            startSaving = true;
        }

        // If it can be saved
        if(startSaving){
            finalMtx.push(matrixToCut[mtxIndex]);
            mtxIndex++;
        }

        // Stop recording when matrix initial time coincides with current ending time
        if(schedule_tbody.matrix[i][0].split(" ")[2] === mtxEnd){
            startSaving = false;
            break;
        }
    }

    return finalMtx.toString();

}




// When classSchedule's page is loaded, count subjects already placed on the right table
function updateCountersFirstTime(){

    // Traverse schedule_tbody2's matrix
    let mtx = schedule_tbody2.matrix;
    for (let row = 0; row < mtx.length; row++) {
        for (let col = 1; col < 8; col++) {
            let stringSubjectPart = mtx[row][col].split("-")[1];
            if(stringSubjectPart !== undefined){
                let stringWithoutColor = stringSubjectPart.split("%")[0].slice(1);

                let check = false;
                for (let i = 0; i < classSubjectCount.length; i++) {
                    if(classSubjectCount[i][0] === stringWithoutColor){
                        check = true;
                    }
                }
                
                if(check){
                    // Increment subjects
                    findSubjectAndIncrement(stringWithoutColor);
                }

            }
        }
    }

}