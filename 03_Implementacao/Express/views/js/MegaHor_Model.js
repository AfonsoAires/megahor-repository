// Schedule class
class Schedule {

  constructor(initial_hours, end_hours, block_size, element) {

    // Schedule image
    this.scheduleAsImage = null;

    this.initial_hours = initial_hours;
    this.end_hours = end_hours;
    this.block_size = block_size;

    // Initialize empty grid
    this.tbody = element;
    
    // Divide string into hours and minutes
    let ini = initial_hours.split(":");
    let end = end_hours.split(":");
    let block = block_size.split(":");

    // From string to int
    // Hours to minutes, and minutes stay the same
    let ini_min = parseInt(ini[0]*60) + parseInt(ini[1])
    let end_min = parseInt(end[0]*60) + parseInt(end[1])
    let block_min = parseInt(block[0]*60) + parseInt(block[1])

    // Divide and create array
    let times_arr = [];
    let aux = 0

    while (aux + ini_min <= end_min) {
      let minutes = (parseInt(ini[1]) + (aux%60));
      let hours = parseInt(ini[0]);
      if(minutes >= 60){
        minutes = minutes - 60;
        hours++; // If minutes exceed 60, then add another hour
      }
      minutes === 0 ? minutes = "00" : (minutes < 10 ? minutes = "0" + minutes : null); // Add a zero before a single digit
      let time_to_save = "" + Math.floor(hours + aux/60) + ":" + (minutes); // Convert back to hours and minutes format and save
      times_arr.push(time_to_save);
      aux = aux + block_min;
    }

    // Add times 2 by 2
    let final_times = []
    for (let i = 1; i < times_arr.length; i++) {
      final_times.push(times_arr[i-1] + " - " + times_arr[i]);
    }

    initial_hours = final_times;


    //Create matrix of nulls to fill later
    this.matrix = [];
    let matrix_row;


    for (let i = 0; i < initial_hours.length + 1; i++) {
      matrix_row = [];

      // Create tr
      let tr = document.createElement("tr");

      // Insert row with th
      let th = document.createElement("th");
      th.appendChild(document.createTextNode(initial_hours[i]));
      tr.appendChild(th);

      // Insert td's for every column
      for (let x = 0; x < 7; x++) {
        let td = document.createElement("td");
        td.setAttribute("onclick", "edit_cell(this)");
        td.setAttribute("class", "align-middle");
        tr.appendChild(td);

        // Add invisible line at the end
        if (i === initial_hours.length) {
          td.setAttribute("style", "display:none;");
          tr.setAttribute("style", "display:none;");
        }
        else{
          // Insert column in matrix ignoring last row
          matrix_row.push("null");
        }
        
        
      }


      // Ignore last row
      if (i !== initial_hours.length) {
        // Insert row in matrix representation
        matrix_row.splice(0, 0, initial_hours[i]);
        this.matrix.push(matrix_row);
      }
      

      // Place content in html (tbody)
      this.tbody.appendChild(tr);

    }


  }

  // Save image in this instance
  set_image(img) {
    this.scheduleAsImage = img;
  }


  //Eliminates all table's children and rewrites the table 
  reset_table(){
    while (this.tbody.firstChild) {
      this.tbody.removeChild(this.tbody.lastChild);
    }
  }
  

  // Methods

  // Note that this is a simplistic toHex appropriate only for this, not negatives or fractionals
  toHex(num, min) {
    var hex = num.toString(16);
    while (hex.length < (min || 0)) {
        hex = "0" + hex;
    }
    return hex;
  }

  // Return hexadecimal, given the rgb value
  getRGB(str) {
    var result = /rgb\((\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*\d+\s*)?\)/.exec(str);
    if (result) {
        return "#" +
               this.toHex(+result[1], 2) +
               this.toHex(+result[2], 2) +
               this.toHex(+result[3], 2);
    }
    return ;
  }

  // Iterate the html table's matrix and save it in this matrix
  update_matrix(){
    // Iterate row
    for (let r = 0; r < this.tbody.children.length - 1; r++) {
      // Iterate columns
      for (let c = 1; c < this.tbody.children[r].children.length; c++) {
        //If table has content, place content in matrix
        if(this.tbody.children[r].children[c].textContent != ""){
          let colorToHex = this.getRGB(this.tbody.children[r].children[c].style.backgroundColor);
          this.matrix[r][c] = this.tbody.children[r].children[c].textContent + "%" +colorToHex;
        }
        //If not, place null
        else{
         this.matrix[r][c] = "null"; 
        }
      }
  }
}

  // Return matrix in string format
  matrix_to_string(){
    return this.matrix.toString();
  }

  // Write this matrix on the html table
  matrix_to_html(matrix){

    let body = this.tbody;
    body.innerHTML = '';

    let matrix_row;
    for (let i = 0; i < matrix.length + 1; i++) {
      matrix_row = [];
      let hour = "";
      let value = "";

      if (i !== matrix.length) {
        hour = matrix[i][0];
      }

      // Create tr
      let tr = document.createElement("tr");
      
      // Insert row with th
      let th = document.createElement("th");
      th.appendChild(document.createTextNode(hour));
      tr.appendChild(th);

      // Insert td's for every column if different of null, if not value is empty string
      for (let x = 0; x < 7; x++) {
        if (i !== matrix.length && matrix[i][x+1] !== "null") {
          value = matrix[i][x+1];
        }else{
          value = "";
        }
        let td = document.createElement("td");
        td.setAttribute("onclick", "edit_cell(this)");
        td.setAttribute("class", "align-middle");

        //Split the value in string and bg-color
        let valueSplited = value.split("%");
        let name = valueSplited[0];
        let bgColor = valueSplited[1];


        td.appendChild(document.createTextNode(name));
        td.setAttribute("style", "background-color: "+ bgColor + ";");
        tr.appendChild(td);

        // Add invisible line at the end
        if (i === matrix.length) {
          td.setAttribute("style", "display:none;");
          tr.setAttribute("style", "display:none;");
        }

        body.appendChild(tr);

    }

  }

}



}
