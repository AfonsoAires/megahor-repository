//####### IMPORTS ##########
const express = require('express'); 
const app = express(); 
app.use(express.json());

//EJS
app.set('view engine', 'ejs');
//
app.use(express.urlencoded({
    extended: true
  }))
app.use('/fonts', express.static('./node_modules/font-awesome/fonts'))
//#######################################################################



//################ SESSION ###########################################
const session = require('express-session');
const one_day = 1000 * 60 * 60 * 24;

app.use(session({
    name: 'sid',
    resave: false,
    saveUninitialized: false,
    secret: 'justASecret',
    cookie: {
        maxAge: one_day,
        sameSite: true,
        secure: false // true for production; false for development
    }
}));
//#######################################################################



//################ DATABASE CONNECTION ###########################################
var mysql = require('mysql');
var con = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root'
});

con.connect(function(err) {
    if (err) throw err
});

//#######################################################################


//PAGES
app.get('/', (req, res) => {
    res.render('Homepage.ejs');
});

app.get('/HomePage.ejs', (req, res) => {
    res.render('Homepage.ejs');
});

app.get('/Login.ejs', (req, res) => {
    res.render('Login.ejs');
});

// Logout
app.get('/LogOut', (req, res) => {
    console.log("LOGOUT");
    req.session.destroy(err => {
        res.clearCookie('sid');
        res.render('Homepage.ejs');
    })
});

// Redirect user, according to the sign up process' result
app.post('/SignUpProcess', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const role = req.body.role;
    const number = req.body.number;

    let query = "";
    // See if user is already registered in the database
    query = "select exists(select email from MEGAHOR.USER where email = '" + email + "')";
    let str = "exists(select email from MEGAHOR.USER where email = '" + email + "')";
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(result[0][str]);
        let page = "";
        // If it doesn't exist
        if(result[0][str] == 0){
            req.session.username = username;
            req.session.role = role;
            req.session.email = email;

            // Insert user parameters in user table
            let user_insert = "insert into MEGAHOR.USER (email, password, name, number) values ('" + email + "', '"+password+"', '"+username+"', "+number+");";
            con.query(user_insert, function (err, result, fields) {
                if (err) throw err;
            });
        
            // Insert role parameters in respective role table
            let specific_insert = "insert into MEGAHOR."+ role +" (email) values ('" + email + "');";
            con.query(specific_insert, function (err, result, fields) {
                if (err) throw err;
                console.log("SPECIFIC INSERT SUCCESSFUL");
                res.render('Selection.ejs', {user: username, role: role});

            });
        }
        else{
            console.log("JA EXISTE");
            res.render("Login.ejs");
        }
        
    });


});


//Find user's role by searching all role tables
function findRole(req, email){
    let queryCoord = 'select email from MEGAHOR.COORDINATOR where email = "' + email + '";';
    let queryTeacher = 'select email from MEGAHOR.TEACHER where email = "' + email + '";';
    let queryStudent = 'select email from MEGAHOR.STUDENT where email = "' + email + '";';
    let queryArray = [queryCoord, queryTeacher, queryStudent];
    let roleArray = ["Coordinator", "Teacher", "Student"];
    
    for(let i = 0; i < queryArray.length; i++){
        con.query(queryArray[i], function (err, result, fields) {
            if (err) throw err;
            if(result[0] !== undefined && result[0] !== null && result[0] !== ""){
                console.log("FUNCAO");
                console.log(roleArray[i]);
                req.session.role = roleArray[i];
            }
        });
    }
}

// Redirect user to the proper page, depending on the authentication's result
app.post('/LoginProcess', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    //find user role
    findRole(req, email);

    //Search user information
    query = 'select * from MEGAHOR.USER where email = "' + email + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(result[0]);
        if(result[0] !== undefined){
            if(result[0].password === password){
                req.session.username = result[0].name;
                req.session.email = result[0].email;
                console.log(req.session);
                res.render('Selection.ejs', {user: req.session.username, role: req.session.role});
            }else{
                res.render('Login.ejs');
            }
        }else{
            res.render('Login.ejs');
        }

            
    });

});



app.get('/Selection.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('Selection.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('Login.ejs');
    }
});

app.get('/ViewSchedules.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('ViewSchedules.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('Login.ejs');
    }
});

app.get('/UnavailabilityStain.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('UnavailabilityStain.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('Login.ejs');
    }
});


//Catch coordinator's request to create/edit a teacher stain
app.post('/UnavailabilityStainCoord.ejs', (req, res) => {    
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        let teacherName = req.body.teacherName;
        console.log(teacherName);
        let teacherID = req.body.teacherID;
        let email = teacherID.split("-")[3];
        console.log(email);
        let teacherSchedule = req.body.teacherSchedule;
        console.log(teacherSchedule);

        res.render('UnavailabilityStainCoord.ejs', {user: req.session.username, role: req.session.role, email: email, name: teacherName, schedule: teacherSchedule});
    }else{
        res.render('Login.ejs');
    }
});


app.get('/CustomClassSchedule.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('CustomClassSchedule.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('Login.ejs');
    }
});

app.get('/CourseManagement.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('CourseManagement.ejs', {user: req.session.username});
    }else{
        res.render('Login.ejs');
    }
});

app.get('/Contact.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('Contact.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('Contact.ejs', {user: null, role: null});
    }
});

app.get('/About.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('About.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('About.ejs', {user: null, role: null});
    }
});


app.get('/Help.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('Help.ejs', {user: req.session.username, role: req.session.role});
    }else{
        res.render('Help.ejs', {user: null, role: null});
    }
});

app.get('/CourseDefinitions.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('CourseDefinitions.ejs', {user: req.session.username});
    }else{
        res.render('Login.ejs');
    }
});


// Return object with all the information relative to each UC -> current subject (0) plus the teacher (1), stain URL (2), stain string (3) and email (4)
app.post('/ClassSchedule.ejs', (req, res) => {
    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        let classContext = req.body.classNameForSchedule;
        console.log(classContext);
        let class_type = req.body.class_type;
        let class_schedule = req.body.class_schedule;
        console.log("AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
        console.log(class_schedule);
        console.log("######################################");


        let splittedContext = classContext.split("-");
    
        let courseID = splittedContext[0];
        let school_year = splittedContext[1];
        let semester = splittedContext[2];
        let className = splittedContext[3];    
        

        let query = 'SELECT * FROM megahor.present_unitname where id_course="' + courseID + '" AND school_year="' + school_year + '" AND semester="' + semester + '" AND name = "' + className + '";';
        con.query(query, function (err, result, fields) {
            if (err) throw err;
            console.log(result);
            let UCs = [];
            let colors = [];
            for (let j = 0; j < result.length; j++) {
                let UC = [result[j].unit_name , result[j].id_unit]
                UCs.push(UC);
                colors.push(result[j].color);
            }

            console.log(colors);
    
    
            let wantedParameters = [];
            //current subject (0) plus the teacher (1), stain URL (2), stain string (3) and email (4)
            let modalJson = {className: className, teachers: wantedParameters, id: classContext, class_type: class_type, class_schedule: class_schedule, colors: colors};

        
            let query = "";
            for (let i = 0; i < UCs.length; i++) {
                let teacherEmails = []; // Eliminate duplicate teachers
                // Find all teachers for a unit
                query = 'SELECT DISTINCT megahor.teaches.email_teach FROM megahor.teaches INNER JOIN megahor.course_unit ON megahor.teaches.id_unit='+ UCs[i][1] +';';
                console.log(query);
                con.query(query, function (err, result, fields) {
                    if (err) throw err;
    
                    for (let x = 0; x < result.length; x++) {
                        // Save teacher
                        teacherEmails.push(result[x].email_teach);
                    }

        
                    // With the teachers, save the necessary parameters
                    for (let z = 0; z < teacherEmails.length; z++) {
    
                        // Get parameters of teacher's latest unavailability stain
                        query = "SELECT * FROM megahor.unavailability_stain where megahor.unavailability_stain.date_time=(SELECT Max(megahor.unavailability_stain.date_time) FROM megahor.unavailability_stain where megahor.unavailability_stain.email='"+teacherEmails[z]+"') AND megahor.unavailability_stain.email='"+teacherEmails[z]+"';";
                        con.query(query, function (err, result, fields) {
                            if (err) throw err;
                            //console.log(result[0]);
                            if (result[0] !== undefined) {
                                query = "SELECT megahor.user.name FROM megahor.user where megahor.user.email='"+teacherEmails[z]+"';";
                                con.query(query, function (err, insideResult, fields) {
                                    if (err) throw err;
                                    // Save parameters
                                    console.log(UCs[i][0]);
                                    console.log(insideResult[0].name);

                                    let itemToAdd = [UCs[i][0], insideResult[0].name, result[0].schedule.toString(), result[0].schedule_url.toString(), teacherEmails[z]];
                                    wantedParameters.push(itemToAdd);
                                    //console.log(modalJson);

                                    if (i === UCs.length - 1 && z === teacherEmails.length-1) {
                                        console.log("entrei");
                                        res.render('ClassSchedule.ejs', modalJson);        
                                    }
                                });
                            }
                        });
                    }
                }); 
            }
        }); 
    }else{
        res.render('Login.ejs');
    } 
});

// Load MegaHor's page (aka Free Schedule's page)
app.get('/MegaHor.ejs', (req, res) => {

    if(req.session.email !== undefined && req.session.email !== null && req.session.email !== ""){
        res.render('MegaHor.ejs', {user: req.session.username});
    }else{
        res.render('MegaHor.ejs', {user: null});
    }

});












//############### COURSE MANAGEMENT #########################

//%%%%%%% DROPDOWNS %%%%%%%%%%%%%

//Get all teacher emails from db
app.get('/teachers', (req, res) => {
    let teacherEmails = [];
    let query = 'SELECT email FROM megahor.teacher;';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        for(let i = 0; i < result.length; i++)
            teacherEmails.push(result[i].email);
        res.json({teacherEmails: teacherEmails});

    });
});

//Get teacher stain from a certain teacher
app.get('/getTeacherStainToManage', (req, res) => {
    console.log(req.query.name);

    // Get parameters of teacher's latest unavailability stain
    let query = "SELECT * FROM megahor.unavailability_stain where megahor.unavailability_stain.date_time=(SELECT Max(megahor.unavailability_stain.date_time) FROM megahor.unavailability_stain where megahor.unavailability_stain.email='"+req.query.name+"') AND megahor.unavailability_stain.email='"+req.query.name+"';";

    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(result);

        //Pass the blobs from array to string
        if(result.length > 0){
            result[0].schedule_url = result[0].schedule_url.toString();
            result[0].schedule = result[0].schedule.toString();
        }


        res.json({result: result});

    }); 
});

//Get all teachers names and emails that are connected to thar unit
app.get('/teachersFromUnit', (req, res) => {
    let teacherEmails = [];
    let teacherNames = [];
    let query = 'SELECT * FROM megahor.course_plan_teachersnames WHERE email="' + req.session.email +'" AND id=' + req.query.course + ' AND school_year= "' + req.query.school_year + '" AND semester= "' + req.query.semester + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        for(let i = 0; i < result.length; i++){
            teacherEmails.push(result[i].email_teacher);
            teacherNames.push(result[i].name);
        }
        res.json({teacherEmails: teacherEmails, teacherNames: teacherNames});
    });
});

//Get all units from that course, school year and semester
app.get('/units', (req, res) => {
    let query = 'SELECT * FROM megahor.course_plan_units WHERE email="' + req.session.email +'" AND id_course=' + req.query.course + ' AND school_year= "' + req.query.school_year + '" AND semester= "' + req.query.semester + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.json({units: result});
    });
});



//%%%%%%% SUBMIT RELATIONSHIPS %%%%%%%%%%%%%

//Post a teacher email in db
app.post('/teacherMails_submit', (req, res) => {
    let name = req.body.teacherName;
    let id = req.body.teacherID;
    let email = req.body.email;

    console.log(req.body);
    console.log(name);
    console.log(email);
    console.log(id);

    let splittedID = id.split("-");

    let courseID = splittedID[0];
    let school_year = splittedID[1];
    let semester = splittedID[2];
    let nameEmailFromID = splittedID[3];

    console.log(courseID);
    console.log(school_year);
    console.log(semester);
    console.log(nameEmailFromID);

    let query = 'SELECT * FROM megahor.teacher where email = "' + email + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        //Check if that teacher already exists in DB
        if(result.length > 0){
            //Check if that teacher already exists in that course, school year, semester
            query = 'SELECT * FROM megahor.exist where email_teacher = "' + email + '"and id_course = "' + courseID + '" and school_year = "' + school_year + '" and semester = "' + semester + '";';
            console.log(query);
            con.query(query, function (err, result2, fields) {
                if (err) throw err;
                console.log(result2);
                if(result2.length > 0){
                    //If it does, error and eliminate
                    console.log("ERRO esse professor já existe nesse contexto, por favor escolher outro email");
                    res.json({nome: "erro"});
                }else{
                        //If not, check the name of the teacher and change it in HTMl to the correct AND put in pertence
                        console.log("MUDAR nome do professor");
                        query = 'SELECT * FROM megahor.user where email = "' + email + '";';
                        console.log(query);
                        con.query(query, function (err, result3, fields) {
                            if (err) throw err;
                            console.log(result3[0].name);
                            query = 'insert into megahor.EXIST (email_teacher, id_course, school_year, semester) values ("' + email + '","' + courseID + '" , "' + school_year + '", "' + semester + '");';
                            console.log(query);
                            con.query(query, function (err, result, fields) {
                                if (err) throw err;
                                console.log(result);
                                res.json({nome: result3[0].name});
                            });
                        });
                    }
        
            });
            console.log(result);
            console.log("tem o email");
        }
        //That teacher doesnt exist in DB yet
        else{
            //Create user and teacher in DB
            console.log(result);
            console.log("nao tem o email");
            query = 'insert into megahor.USER (email, password, name, number) values ("' + email + '","pass" , "' + name + '", "1000");';
            console.log(query);
            con.query(query, function (err, result, fields) {
                if (err) throw err;
                query = 'insert into megahor.TEACHER (email) values ("' + email + '");';
                console.log(query);
                con.query(query, function (err, result, fields) {
                    if (err) throw err;
                    query = 'insert into megahor.EXIST (email_teacher, id_course, school_year, semester) values ("' + email + '","' + courseID + '" , "' + school_year + '", "' + semester + '");';
                    console.log(query);
                    con.query(query, function (err, result, fields) {
                        if (err) throw err;
                        console.log(result);
                        res.json({nome: "criado"});
                    });
                });
            }); 
        }
    }); 
});

//Post a teacher email in a unit
app.post('/unitTeachers_submit', (req, res) => {
    let teacher = req.body.teacher;
    let context = req.body.contextUnits;

    console.log(teacher);
    console.log(context);

    let splittedTeacherName = teacher.split("-");
    let splittedContext = context.split("-");

    let email = splittedTeacherName[1];
    let courseID = splittedContext[0];
    let school_year = splittedContext[1];
    let semester = splittedContext[2];
    let unitID = splittedContext[3];


    let query = 'insert ignore into megahor.TEACHES (email_teach, school_year, semester, id_course, id_unit) values ("' + email + '","' + school_year + '" , "' + semester + '", "' + courseID + '", "' + unitID + '");';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.status(200).send();
    }); 
});

//Post a unit in a class
app.post('/classesUnits_submit', (req, res) => {
    let unitName = req.body.unit;
    let context = req.body.contextClasses;

    console.log(unitName);
    console.log(context);

    let splittedUnitName = unitName.split("-");
    let splittedContext = context.split("-");

    let unitID = splittedUnitName[1];
    let courseID = splittedContext[0];
    let school_year = splittedContext[1];
    let semester = splittedContext[2];
    let className = splittedContext[3];

    
    let query = 'insert ignore into megahor.PRESENT (name, school_year, semester, id_course, id_unit) values ("' + className + '","' + school_year + '" , "' + semester + '", "' + courseID + '", "' + unitID + '");';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        //console.log(result);
        res.status(200).send();
    }); 


});


//%%%%%%%  SET TREE VIEW  %%%%%%%%%%%%%

//Get all teachers, units and classes 
app.get('/setLists', (req, res) => {
    query = 'SELECT * FROM megahor.course_plan_information WHERE email="' + req.session.email + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        //console.log(result)
        query = 'SELECT * FROM megahor.course_plan_teachersnames WHERE email="' + req.session.email + '";';
        con.query(query, function (err, result2, fields) {
            if (err) throw err;
            //console.log(result2);
            query = 'SELECT * FROM megahor.course_plan_units WHERE email="' + req.session.email + '";';
            con.query(query, function (err, result3, fields) {
                if (err) throw err;
                //console.log(result2);
                query = 'SELECT * FROM megahor.course_plan_classes WHERE email="' + req.session.email + '";';
                con.query(query, function (err, result4, fields) {
                    if (err) throw err;
                    //console.log(result4);
                    res.json({courses: result, teachers: result2, units: result3, classes: result4});
                }); 
            }); 
        }); 
    }); 
});


//Get a teacher stain
app.get('/getTeacherStain', (req, res) => {
    console.log(req.query.course);
    console.log(req.query.school_year);
    console.log(req.query.semester);
    console.log(req.query.name);


    let query = 'SELECT * FROM megahor.course_information_names WHERE id=' + req.query.course + ' AND school_year= "' + req.query.school_year + '" AND semester= "' + req.query.semester + '" AND id_unit= ' + req.query.name + ';';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(result);
        res.json({result: result});

    }); 
});

//Get all teachers from a certain unit
app.get('/setTeacherListToUnits', (req, res) => {
    let query = 'SELECT * FROM megahor.course_information_names WHERE id=' + req.query.course + ' AND school_year= "' + req.query.school_year + '" AND semester= "' + req.query.semester + '" AND id_unit= ' + req.query.name + ';';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(result);
        res.json({result: result});

    }); 
});

//Get all units from a certain class
app.get('/setUnitsListToClasses', (req, res) => {
    let teacher = [];
    let teachers = [];
    //Get hours definitions from that class
    let query = 'SELECT * FROM megahor.course_plan where id_course="' + req.query.course + '" AND school_year="' + req.query.school_year + '" AND semester="' + req.query.semester + '";';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        let definitions = [];
        console.log(result);
        if(result[0].initial_hour !== null){
            definitions.push(result[0].initial_hour);
            definitions.push(result[0].final_hour);
            definitions.push(result[0].block_duration);
        }
        //Get all units name from that class
        let query = 'SELECT * FROM megahor.present_unitname where id_course="' + req.query.course + '" AND school_year="' + req.query.school_year + '" AND semester="' + req.query.semester + '" AND name = "' + req.query.name + '";';
        console.log(query);
        con.query(query, function (err, result, fields) {
            if (err) throw err;
            console.log("result" + result[0]);
            console.log("result len" + result.length);
            //Get class schedule
            let query = 'SELECT * FROM megahor.class where id_course="' + req.query.course + '" AND school_year="' + req.query.school_year + '" AND semester="' + req.query.semester + '" AND class_name = "' + req.query.name + '";';
            console.log(query);
            con.query(query, function (err, result2, fields) {
                if (err) throw err;
                //Transform from buffer to String
                result2[0].schedule_url = result2[0].schedule_url.toString();
                result2[0].schedule = result2[0].schedule.toString();

                //if that class have some units check teacher in each unit
                if(result.length > 0){
                    for(let i = 0; i < result.length; i++){
                        query = 'SELECT * FROM megahor.course_information_names WHERE id=' + req.query.course + ' AND school_year= "' + req.query.school_year + '" AND semester= "' + req.query.semester + '" AND id_unit= ' + result[i].id_unit + ';';
                        console.log(query);
                        con.query(query, function (err, result3, fields) {
                            if (err) throw err;
                            console.log(result3);
                            teacher = [];
                            for(let j = 0; j < result3.length; j++){
                                teacher.push(result3[j].name);
                            }
                            teachers.push(teacher);
        
                            if(i === result.length-1){
                                console.log(teachers);
                                res.json({result: result, class_type: result2, teachers: teachers, definitions: definitions});
                            }
                        }); 
                    }
                }else{
                    res.json({result: result, class_type: result2, teachers: teachers});
                }
            }); 
        }); 
    });
});


//Create a class in a certain context
app.post('/createClass', (req, res) => {
    let query = 'insert into megahor.CLASS (class_name, schedule, schedule_url, school_year, semester, id_course, class_type) values("' + req.body.name + '", "", "",  "' + req.body.school_year + '", "' + req.body.semester + '", "' + req.body.course + '" , "day_class");';

    console.log(query);

    con.query(query, function (err, result, fields) {
        if (err){
            res.status(500).send();
        } else{
            res.status(200).send();
        }
    }); 

});

//Check the max id from units
app.get('/checkUnitID', (req, res) => {
    let query = "SELECT MAX(ID) FROM megahor.course_unit";
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.json({result: result});
    }); 
});

//Create a unit ina certain context
app.post('/createUnit', (req, res) => {
    console.log(req.body);

    query = 'insert into megahor.COURSE_UNIT (id, unit_name, id_course, school_year, semester, color) values("' + req.body.id + '", "' + req.body.name + '", "' + req.body.course + '", "' + req.body.school_year + '", "' + req.body.semester + '", "' + req.body.color + '");';
        con.query(query, function (err, result2, fields) {
            if (err) throw err;
            res.status(200).send();
        }); 
});



//%%%%%%%  DELETE REALTIONS AND FROM TREE VIEW  %%%%%%%%%%%%%

//Delete class from tree view
app.post('/deleteClass', (req, res) => {
    console.log(req.body);
    query = 'DELETE FROM megahor.belongs WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND name ="' + req.body.entity + '";';
    console.log(query);    
    con.query(query, function (err, result2, fields) {
        if (err) throw err;
        query = 'DELETE FROM megahor.present WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND name ="' + req.body.entity + '";';
        console.log(query);    
        con.query(query, function (err, result2, fields) {
            if (err) throw err;
            query = 'DELETE FROM megahor.class WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND class_name ="' + req.body.entity + '";';
            console.log(query);    
            con.query(query, function (err, result2, fields) {
                if (err) throw err;
                res.status(200).send();
            }); 
        }); 
    }); 
});

//Delete unit from tree view
app.post('/deleteUnit', (req, res) => {
    console.log(req.body);
    query = 'DELETE FROM megahor.subject WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND id_unit ="' + req.body.entity + '";';
    console.log(query);    
    con.query(query, function (err, result2, fields) {
        if (err) throw err;
        query = 'DELETE FROM megahor.present WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND id_unit ="' + req.body.entity + '";';
        console.log(query);    
        con.query(query, function (err, result2, fields) {
            if (err) throw err;
            query = 'DELETE FROM megahor.teaches WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND id_unit ="' + req.body.entity + '";';
            console.log(query);    
            con.query(query, function (err, result2, fields) {
                if (err) throw err;
                query = 'DELETE FROM megahor.course_unit WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND id ="' + req.body.entity + '";';
                console.log(query);    
                con.query(query, function (err, result2, fields) {
                    if (err) throw err;
                    res.status(200).send();
                }); 
            }); 
        }); 
    }); 
});

//Delete teacher from tree view
app.post('/deleteTeacher', (req, res) => {
    console.log(req.body);
    query = 'DELETE FROM megahor.teaches WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND email_teach ="' + req.body.entity + '";';
    console.log(query);    
    con.query(query, function (err, result2, fields) {
        if (err) throw err;
        query = 'DELETE FROM megahor.exist WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND email_teacher ="' + req.body.entity + '";';
        console.log(query);    
            con.query(query, function (err, result2, fields) {
                if (err) throw err;
                res.status(200).send();
            }); 
    }); 
});

//Delete units teacher
app.post('/deleteTeacherInsideUnit', (req, res) => {
    console.log(req.body);
    query = 'DELETE FROM megahor.teaches WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND email_teach ="' + req.body.toElimate + '"AND id_unit ="' + req.body.fromEliminate + '";';
    console.log(query);    
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.status(200).send();
    }); 
});

//Delete classes units
app.post('/deleteUnitInsideClass', (req, res) => {
    console.log(req.body);
    query = 'DELETE FROM megahor.present WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '"AND name ="' + req.body.fromEliminate + '"AND id_unit ="' + req.body.toElimate + '";';
    console.log(query);    
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.status(200).send();
    }); 
});

//Save a class schedule
app.post('/saveClassSchedule', (req, res) => {
    console.log(req.body);


    query = "INSERT INTO megahor.class (class_name, schedule, schedule_url,school_year,semester,id_course, class_type) VALUES('"+req.body.entity+"', '"+req.body.schedule+"', '"+req.body.scheduleURL+"' , '"+req.body.school_year+"' , '"+req.body.semester+"' , '"+req.body.course+"' , '"+req.body.class_type+"') ON DUPLICATE KEY UPDATE schedule_url='"+req.body.scheduleURL+"', schedule='"+req.body.schedule+"';";
    console.log(query);    
    con.query(query, function (err, result2, fields) {
        if (err) throw err;
        console.log("saved");
        res.status(200).send();
    }); 
});

//Save a teacher stain
app.post('/saveStain', (req, res) => {
    let matrix = req.body.matrix;
    let stain = req.body.stain;
    let teacherName = req.body.teacherName;
    let teacherEmail = req.body.teacherEmail;

    console.log("OLAAAAAAAAA");
    console.log(teacherEmail);



    query = "INSERT INTO megahor.unavailability_stain (date_time, schedule, schedule_url, email) VALUES(curdate(), '"+matrix+"', '"+stain+"', '"+req.session.email+"') ON DUPLICATE KEY UPDATE schedule_url='"+stain+"', schedule='"+matrix+"', date_time=curdate();";
    console.log(query);
    con.query(query, function (err, insideResult, fields) {
        if (err) throw err;

        res.json({stain: stain, matrix: matrix, teacherName: teacherName});
    });
});

//Save teacher stain by coordinator
app.post('/saveStainCoord', (req, res) => {
    let matrix = req.body.matrix;
    let stain = req.body.stain;
    let teacherName = req.body.teacherName;
    let teacherEmail = req.body.teacherEmail;

    console.log(teacherEmail);

    query = "INSERT INTO megahor.unavailability_stain (date_time, schedule, schedule_url, email) VALUES(curdate(), '"+matrix+"', '"+stain+"', '"+teacherEmail+"') ON DUPLICATE KEY UPDATE schedule_url='"+stain+"', schedule='"+matrix+"', date_time=curdate();";
    console.log(query);
    con.query(query, function (err, insideResult, fields) {
        if (err) throw err;

        res.json({stain: stain, matrix: matrix, teacherName: teacherName});
    });
});





//#################### course definitions #################################
//Get all courses plan from that coordinator
app.get('/setCoursePlan', (req, res) => {
    query = 'SELECT * FROM megahor.course_plan_information WHERE email="' + req.session.email + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.json({courses: result}); 
    }); 
});

//Update hours from a certain course plan
app.post('/updateCoursePlanInfo', (req, res) => {
    console.log(req.body.context);
    console.log(req.body.initial_hour);
    console.log(req.body.final_hour);
    console.log(req.body.block_duration);
    console.log(req.body.block_for_unit);

    let splittedContext = req.body.context.split("-");
    
    let courseID = splittedContext[0];
    let school_year = splittedContext[1];
    let semester = splittedContext[2];

    query = 'UPDATE MEGAHOR.course_plan SET initial_hour = "' + req.body.initial_hour + '", final_hour="' + req.body.final_hour + '", block_duration = "' + req.body.block_duration + '", block_for_unit ="' + req.body.block_for_unit  + '" WHERE id_course="' + courseID + '"AND school_year = "' + school_year + '"AND semester ="' + semester + '";';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err){
            res.render('Login.ejs');
        }else{
            res.render('CourseDefinitions.ejs', {user: req.session.username});
        } 
    });
});

//Toogle between day class and night class, if that course has definitions send to client
app.post('/setClassType', (req, res) => {
    console.log(req.body.course);
    console.log(req.body.school_year);
    console.log(req.body.semester);
    console.log(req.body.entity);
    console.log(req.body.value);


    query = 'UPDATE MEGAHOR.class SET class_type = "' + req.body.value  + '" WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '";';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;

        query = 'SELECT  * FROM megahor.course_plan WHERE id_course="' + req.body.course + '"AND school_year = "' + req.body.school_year + '"AND semester ="' + req.body.semester + '";';
        console.log(query);
        con.query(query, function (err, result, fields) {
            if (err) throw err;
            let definitions = [];
            if(result[0].initial_hour !== null){
                definitions.push(result[0].initial_hour);
                definitions.push(result[0].final_hour);
                definitions.push(result[0].block_duration);
            }
            //res.status(200).send();
            res.json({definitions: definitions});
        }); 

    }); 
});

//get a certain class definitions
app.get('/getClassDefinitions', (req, res) => {
    console.log(req.query.course);
    console.log(req.query.school_year);
    console.log(req.query.semester);
    console.log(req.query.entity);

    let query = 'SELECT * FROM megahor.course_plan WHERE id_course="' + req.query.course + '"AND school_year = "' + req.query.school_year + '"AND semester ="' + req.query.semester + '";';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        res.json({result: result});

    });

});






//#################### view schedules #################################
//Get all teacher unavailability stains
app.get('/loadTeachersSchedules', (req, res) => {
    let query = 'SELECT * FROM megahor.unavailability_stain WHERE email="' + req.session.email + '";';
    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        for(let i = 0; i < result.length; i++){
            result[i].schedule = result[i].schedule.toString();
            result[i].schedule_url = result[i].schedule_url.toString();
        }

        res.json({result: result});
    });    
});

//Get all classes schedules from a certain teacher
app.get('/loadCoordSchedules', (req, res) => {
    let query = 'SELECT * FROM megahor.course_plan_classes WHERE email="' + req.session.email + '";';
    con.query(query, function (err, result, fields) {
        if (err) throw err;
        //console.log(result);
        for(let i = 0; i < result.length; i++){
            result[i].schedule = result[i].schedule.toString();
            result[i].schedule_url = result[i].schedule_url.toString();
        }

        res.json({result: result});
    });  
});


//Show a class schedule from a certain context (course, school year, semester)
app.get('/ShowScheduleCoord/:context', (req, res) => {

    let classContext = req.params.context;
    let splittedContext = classContext.split("-");
    
    let courseID = splittedContext[5];
    let courseName = splittedContext[0];
    let school_yearFirst = splittedContext[1];
    let school_yearSecond = splittedContext[2];
    let school_year = school_yearFirst + "/" + school_yearSecond;
    let semester = splittedContext[3];
    let className = splittedContext[4];

    let str = courseName + "-" + school_year + "-" + semester + "-" + className;


    let query = 'SELECT schedule_url FROM megahor.class where id_course="' + courseID + '" AND school_year="' + school_year + '" AND semester="' + semester + '" AND class_name = "' + className + '";';
        console.log(query);
        con.query(query, function (err, result, fields) {
            let url = result[0].schedule_url.toString();
            //Pass from buffer to string
            res.render('ShowSchedule.ejs', {name: str, URL: url, user: req.session.username, role: req.session.role});
        }); 
}); 

//Show stain from a certain teacher
app.get('/ShowScheduleTeacher/:context', (req, res) => {

    let context = req.params.context;
    console.log(context);

    let splittedContext = context.split("&");
    let email = splittedContext[0];
    let date = splittedContext[1];

    let str = "Unavailability stain - " + date.split(" ")[0];

    let query = 'SELECT schedule_url FROM megahor.unavailability_stain WHERE email="' + email + '" AND date_time="' + date + '";';
        console.log(query);
        con.query(query, function (err, result, fields) {
            //Pass from buffer to string
            let url = result[0].schedule_url.toString();
            res.render('ShowSchedule.ejs', {name: str, URL: url, user: req.session.username, role: req.session.role});
        }); 
    
}); 



//###################################################################






//################### CSS, JS & IMAGES ##########################################

//CSS
app.get('/Homepage.css', (req, res) => {
    res.sendFile('views/css/Homepage.css', { root: '.' });
});

app.get('/MegaHorCSS.css', (req, res) => {
    res.sendFile('views/css/MegaHorCSS.css', { root: '.' });
});

app.get('/font-awesome.min.css', (req, res) => {
    res.sendFile('views/css/font-awesome.min.css', { root: '.' });
});



//JS
app.get('/Megahor.js', (req, res) => {
    res.sendFile('views/js/Megahor.js', { root: '.' });
});

app.get('/MegaHor_Model.js', (req, res) => {
    res.sendFile('views/js/MegaHor_Model.js', { root: '.' });
});

app.get('/MegaHor_Controller.js', (req, res) => {
    res.sendFile('views/js/MegaHor_Controller.js', { root: '.' });
});



//IMAGES
app.get('/logo4.png', (req, res) => {
    // Send initial page
    res.sendFile('imgs/logo4.png', { root: '.' });
});

app.get('/Homepage2.jpg', (req, res) => {
    // Send initial page
    res.sendFile('imgs/Homepage2.jpg', { root: '.' });
});

app.get('/profile.png', (req, res) => {
    // Send initial page
    res.sendFile('imgs/profile.png', { root: '.' });
});

app.get('/ViewSchedules.jpg', (req, res) => {
    // Send initial page
    res.sendFile('imgs/ViewSchedules.jpg', { root: '.' });
});

app.get('/CourseManagement.jpg', (req, res) => {
    // Send initial page
    res.sendFile('imgs/CourseManagement.jpg', { root: '.' });
});

app.get('/Homepage4.jpg', (req, res) => {
    // Send initial page
    res.sendFile('imgs/Homepage4.jpg', { root: '.' });
});

app.get('/plus.png', (req, res) => {
    // Send initial page
    res.sendFile('imgs/plus.png', { root: '.' });
});

app.get('/contact.jpg', (req, res) => {
    // Send initial page
    res.sendFile('imgs/contact.jpg', { root: '.' });
});

app.get('/definitions.jpg', (req, res) => {
    // Send initial page
    res.sendFile('imgs/definitions.jpg', { root: '.' });
});


app.get('/logo-isel.png', (req, res) => {
    // Send initial page
    res.sendFile('imgs/logo-isel.png', { root: '.' });
});





const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening on port ${port}...`));